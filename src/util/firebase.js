import  firebase from "firebase/app";
import "firebase/auth";
import 'firebase/firestore'
import {authState} from 'rxfire/auth';
import {collectionData} from 'rxfire/firestore';
import{filter} from 'rxjs/operators';

const app = firebase.initializeApp({
        apiKey: "AIzaSyB05Feey3Eej3rb-QKKxxEh46OOnTgm-nI",
        authDomain: "reactivate-72f8a.firebaseapp.com",
        projectId: "reactivate-72f8a",
        storageBucket: "reactivate-72f8a.appspot.com",
        messagingSenderId: "217371061033",
        appId: "1:217371061033:web:c003f3c5920a73cf4ca538",
        measurementId: "G-23WG933R2W"
    });
const firestore=firebase.firestore(app);
const auth=firebase.auth(app);
const loggedIn$=authState(auth).pipe(filter(user=>!!user));
export {app,auth,firestore,collectionData};
    export default firebase;