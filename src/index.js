import React  from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from 'react-router-dom';
import * as serviceWorker from './vistas/serviceWorker';
ReactDOM.render(
  <BrowserRouter>
  <React.StrictMode>
    <App/>
  </React.StrictMode>
  </BrowserRouter>,
  document.getElementById('root')
);
//reportWebVitals();
serviceWorker.unregister();

/*import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import * as serviceWorker from './vistas/serviceWorker';
import Login from './vistas/Login'

import Show from './componentes/Show';

ReactDOM.render(
  <Router>
    <div>

      <Route exact path='/' component={Login} />

      <Route path='/show/:id' component={Show} />
    </div>
  </Router>,
  document.getElementById('root')
);
serviceWorker.register(); */