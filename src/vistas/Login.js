import './estilo.css';
import logo from '../dragon1.gif';
import React, { Component, useCallback, useContext, useState } from 'react';
import 'firebase/auth';
import firebaseConfig from '../util/firebase';
import Rutas from '../rutas/rutas';
import { Redirect, useHistory, withRouter } from 'react-router-dom'
import { AuthContext } from "../util/Auth";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import {useFirebaseApp,useUser} from 'reactfire'; 
import { useForm } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import Login from './Inicio';
import { Home } from '@material-ui/icons';
const eye = <FontAwesomeIcon icon={faEye} />;



class Example extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        modal: false,
        passwordShown:false,  
        };
        
        this.toggle = this.toggle.bind(this);
        this.passwordShown=this.togglePasswordVisiblity.bind(this);
    }
    togglePasswordVisiblity() {
        this.setState({
            passwordShown: !this.state.passwordShown
            ?true
            :false
        })
       console.log(this.state.passwordShown) 
    }
    toggle() {
        this.setState({
        modal: !this.state.modal
        });
    }

    handleLogin = async event => {
            event.preventDefault();
            const { email, password } = event.target.elements;
            if(email.value==='libreriafamey@gmail.com'){
            try {
                await firebaseConfig.auth().signInWithEmailAndPassword(email.value, password.value);
              <Rutas/>
               // history.push("/inicio");
            } catch (error) {
                swal({
                    title: "Verifica!",
                    text: "!!!" + error,
                    icon: "warning",
                    button: "Aceptar",
                });
            }
        }else{
            swal({
                title: "No permitido para su tipo de usuario!!!",
                text: "Ingrese sus datos nuevamente" ,
                icon: "warning",
                button: "Ok",
            });

        }
        }
        
    



    render() {
return (
    
            <div className="container">
                <div className="row">
                    <div className="col-md-5 mx-auto">
                        <div id="first">
                            <div className="myform form ">
                                <div className="logo mb-3">
                                    <div className="col-md-12 text-center">
                                        <h1>Ingresar</h1>
                                    </div>
                                </div>
                                <form method="post" name="login" onSubmit={this.handleLogin}>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1" className="label">Email </label><br></br>
                                        <input type="email" name="email" className="input" id="email" aria-describedby="emailHelp" placeholder="Enter email"/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1" className="label">Contraseña</label><br></br>
                                        <input type="password" name="password" id="password" className="input2" aria-describedby="emailHelp" placeholder="Enter Password" 
                                        type={this.state.passwordShown ? "text" : "password"} />
                                         
                                        <i className="ojo" onClick={() => this.togglePasswordVisiblity() }>{eye}</i>    
                                    </div>
                                    <div className="form-group">
                                        <p className="text-center">Al ingresar acepta los terminos de uso</p><p className="terminos"> <a  onClick={this.toggle} >Terminos de Uso</a></p>
                                    </div>
                                    <div className="col-md-12 text-center ">
                                        <button  className=" btn btn-block mybtn btn-primary tx-tfm" >Ingresar</button>
                                    </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br></br>
                <br></br>
                <div>
			  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'/>
 
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="Modal">
          <ModalHeader toggle={this.toggle}>Terminos de Uso</ModalHeader>
          <ModalBody>
          Este sitio web está destinado a la venta y control de inventario de la librería y papelería Famey el uso es exclusivo de esta entidad, además esta web no permite el registro de usuarios pues es netamente para la administración
          </ModalBody>
          <ModalFooter>
            <Button color='primary' onClick={this.toggle}>Aceptar</Button>{' '}
            </ModalFooter>
        </Modal>
      </div>
      </div>
    );
  }
}
export default Example
/*<div className="col-md-12 ">
                                    <div className="login-or">
                                        <hr className="hr-or" />
                                        <span className="span-or">or</span>
                                    </div>
                                </div>
                                <div className="col-md-12 mb-3">

                                    <p className="text-center">

                                        <a href="#" className="sas"><i>
                                        </i>  Usar Google  </a>

                                    </p>
                                </div>
                                <div className="form-group">
                                    <p className="text-center">No tienes Cuenta? <a href="#" id="signup">Registrate Aqui</a></p>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div id="second">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    <h1 >Signup</h1>
                                </div>
                            </div>
                            <form action="#" name="registration">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">First Name</label>
                                    <input type="text" name="firstname" className="form-control" id="firstname" aria-describedby="emailHelp" placeholder="Enter Firstname" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Last Name</label>
                                    <input type="text" name="lastname" className="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Lastname" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" className="form-control" id="emailadd" aria-describedby="emailHelp" placeholder="Enter email" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Password</label>
                                    <input type="password" name="password" id="passwordadd" className="form-control" aria-describedby="emailHelp" placeholder="Enter Password" />
                                </div>
                                <div className="col-md-12 text-center mb-3">
                                    <button type="submit" className=" btn btn-block mybtn btn-primary tx-tfm">Get Started For Free</button>
                                </div>
                                <div className="col-md-12 ">
                                    <div className="form-group">
                                        <p className="text-center"><a href="#" id="signin">Already have an account?</a></p>
                                    </div>
                                </div>
 */


 /*esto es para login usando export default
 constructor(props) {
        super(props);

        this.state = {
            abrirmodal: false
        };

        this.delta = this.delta.bind(this);
    }
    handleLogin = useCallback(
        async event => {
            event.preventDefault();
            const { email, password } = event.target.elements;
            try {
                await firebaseConfig.auth().signInWithEmailAndPassword(email.value, password.value);

                history.push("/inicio");
            } catch (error) {
                swal({
                    title: "Verifica!",
                    text: "!!!" + error,
                    icon: "warning",
                    button: "Aceptar",
                });
            }
        },
        
    );
    abrirmodal() {
        this.setState({ abrirmodal: false });
    };
 
 */
/*[email,setEmail]=useState('');
      [password,setPassword]=useState('');
      
       submit=()=>{
         console.log(email,password)
      } */



      /*
      import React, { useState } from "react";

import "./estilo.css";
import { useForm } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
const eye = <FontAwesomeIcon icon={faEye} />;

export default function App() {
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  const { register, handleSubmit } = useForm();
  const onSubmit = data => {
    console.log(data);
  };

  return (
   
        <i onClick={togglePasswordVisiblity}>{eye}</i>{" "}
     
  );
}

      */