/*import React from 'react';
import 'firebaseui/dist/firebaseui';
import Firebase from '../util/Firebase';
 
class Login extends React.Component {​​​​​​​​
    state = {​​​​​​​​
        autenticado: false,
        usuario: "",
        firebase: null
    }​​​​​​​​
    componentDidMount() {​​​​​​​​const firebase = new Firebase(); firebase.auth.onAuthStateChanged(authUser => {​​​​​​​​ authUser ? this.setState({​​​​​​​​ autenticado: true, usuario: firebase.auth.currentUser.email, firebase: firebase }​​​​​​​​) : firebase.firebaseui.start("@firebaseui-auth-container"), {​​​​​​​​ signInSuccessUrl: "/", credentialHelper: "none", callback: {​​​​​​​​ signInSuccessWithAuthResult: (authResult, redirectUrl) => {​​​​​​​​ this.setState({​​​​​​​​ autenticado: true, usuario: firebase.auth.currentUser.email, firebase: firebase }​​​​​​​​); return false; }​​​​​​​​ }​​​​​​​​, signInOption: [{​​​​​​​​ provider: firebase.autorization.EmailAuthProvider.PROVIDER_ID }​​​​​​​​] }​​​​​​​​ }​​​​​​​​)}​​​​​​​​
    render() {​​​​​​​​
        return this.state.autentication
            ? (
                <React.fragment>
                    <div>Usuario Logeado {​​​​​​​​this.state.usuario}​​​​​​​​</div>
                    <a href="#" onClick={​​​​​​​​() => {​​​​​​​​ this.state.firebase.auth.signOut().then(success => {​​​​​​​​ this.state({​​​​​​​​ autenticado: false }​​​​​​​​) }​​​​​​​​) }​​​​​​​​}​​​​​​​​>Salir</a>
                </React.fragment>
            )
            : <div id="firebaseui-auth-container">
 
            </div>
    }​​​​​​​​
 
}​​​​​​​​
 
export default Login;
*/
/*
//use
import React, {​​​​​​​​ Component }​​​​​​​​ from 'react';
import withFirebaseAuth from 'react-with-firebase-auth'
import  firebase from 'firebase/app';
import 'firebase/auth';
import firebaseConfig from '../util/firebase';
 
const firebaseApp = firebase.initializeApp(firebaseConfig);
class Login extends Component {​​​​​​​​
    render() {​​​​​​​​
        const {​​​​​​​​
            user,
            signOut,
            signInWithGoogle,
        }​​​​​​​​ = this.props;
 
        return (
            <div>
                {​​​​​​​​
                    user
                        ? <p>Hello, {​​​​​​​​user.displayName}​​​​​​​​</p>
                        : <p>Please sign in.</p>
                }​​​​​​​​
 
                {​​​​​​​​
                    user
                        ? <button onClick={​​​​​​​​signOut}​​​​​​​​>Sign out</button>
                        : <button onClick={​​​​​​​​signInWithGoogle}​​​​​​​​>Sign in with Google</button>
                }​​​​​​​​
            </div>
        );
    }​​​​​​​​
}​​​​​​​​
 
const firebaseAppAuth = firebaseApp.auth();
 
const providers = {​​​​​​​​
    googleProvider: new firebase.auth.GoogleAuthProvider(),
}​​​​​​​​;
 
export default withFirebaseAuth({​​​​​​​​
    providers,
    firebaseAppAuth,
}​​​​​​​​)(Login);
*/
import './App.css';
import 'antd/dist/antd.css';
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import Menup from '../componentes/menup';
import Header from '../componentes/header';
import Case from "../componentes/case";
import Section from '../componentes/section';

import "firebase/auth";
import { render } from '@testing-library/react';
class  Inicio extends Component   {
    constructor(props) {
        super(props);
        this.state = {opc: null};
        
      }
   
     handleChange = (valo) => {
        
       console.log(this.props.opc)
         
      }
    render(){
        
    return(
        
            <>
           
                <header className="header"><Header /></header>
                <main><div className="menup"><Menup /></div></main>
                <section><Case status={this.state.opc}/></section>
            </>
    )
    }
    
};
export default Inicio;