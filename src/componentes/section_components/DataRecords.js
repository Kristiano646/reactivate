import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import TableContainer from '@material-ui/core/TableContainer';
import firebase from 'firebase/app';
import 'firebase/database';

class DataRecords extends Component {




    render() {
        return (
            <>

                <TableContainer className="tablaing">
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Nombres Producto</TableCell>
                                <TableCell align="left">Stock</TableCell>
                                <TableCell align="right">Costo Compra</TableCell>
                                <TableCell align="right">Costo Venta</TableCell>
                                <TableCell align="right">Detalle</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {


                                !this.props.NoData
                                    ? firebase.database().ref().child('Producto')
                                        .on('value', (snapshot) => {

                                            const data = snapshot.val();
                                            return (<TableRow>
                                                <TableCell component="th" scope="row">{data}</TableCell>
                                                <TableCell align="left">ok</TableCell>
                                                <TableCell align="right"> <Button size="small" variant="outlined" color="primary" >Ver/Editar </Button><Button style={{ marginLeft: "10px" }}
                                                    size="small" variant="outlined" color="secondary">Eliminar</Button>
                                                </TableCell>
                                            </TableRow>
                                            )



                                        }) : 'sin datos'}

                        </TableBody>
                    </Table>
                </TableContainer>
            </>
        )
    }
}
export default DataRecords;

/*
<TableBody> {!this.props.Nodata ?this.props.children.Employees.map(row => {


                    const {employee} = row;
                    return (<TableRow key={employee.id}>
                        <TableCell component="th" scope="row">{employee.NombrePro}</TableCell>
                        <TableCell align="left">{employee.NombrePro}</TableCell>
                        <TableCell align="right"> <Button size="small" variant="outlined" color="primary" onClick={() => this.props.handleEdit(employee.id)}>Ver/Editar </Button><Button style={{marginLeft: "10px"}}
                            size="small" variant="outlined" color="secondary" onClick={() => this.props.handleDelete(employee.id)}>Eliminar</Button>
                        </TableCell>
                    </TableRow>
                    )
                })
                : 'Sin Datos'}</TableBody>


*/