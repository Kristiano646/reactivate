import { React, Component } from 'react';
import Table from '@material-ui/core/Table';
import { withStyles, createStyles, createMuiTheme } from '@material-ui/core/styles';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import firebase from 'firebase/app';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import 'firebase/database';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import Home from '../../vistas/home';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';
import BeenhereIcon from '@material-ui/icons/Beenhere';
import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import Tooltip from '@material-ui/core/Tooltip';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import { date } from 'date-fns/locale/af';
import { text } from '@fortawesome/fontawesome-svg-core';
class factura extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalpag: '',
      modalEditar: true,
      data: [],
      clienteP: [],
      term: 'sin datos',
      producto: [],
      modalEditar: false,
      modalCli: false,
      Fecha: new Date(),
      id: 0,
      formc: {
        id: '',
        Nombre: 'Consumidor Final',
        cedula: '0000000000',
        email: 'xxxxx@email.com',
        direccion: 'No aplica',
        telefono: '000000000',
      },
      idc: '',
      ruc: '0602583833001',
      telf: '032587720',
      direccionE: 'Homero Idrovo y Nicasio Safady',
      date: new Date(),
      titleButton: 'Enviar',
      nodata: false,
      ventaH: [],
      vent:[],
      modalVender: false,
      iva: 0,
      cliente:[],
      idPreVenta:0,
    }
  }
  toggle = this.toggle.bind(this);
  validar(cedulaval) {
    console.log(cedulaval)
    var cad = cedulaval
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;

    if (cad !== "" && longitud === 10) {
      for (var i = 0; i < longcheck; i++) {
        if (i % 2 === 0) {
          var aux = cad.charAt(i) * 2;
          if (aux > 9) aux -= 9;
          total += aux;
        } else {
          total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
        }
      }

      total = total % 10 ? 10 - total % 10 : 0;

      if (cad.charAt(longitud - 1) == total) {
        console.log("vale")
        this.state.formc.cedula = cedulaval;
      } else {
        console.log("no vale")
        swal({
          title: "Numero de Cédula no Valido",
          text: "Ingreselo nuevamente ",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
      }
    }
  }
  componentWillMount() {
    this.peticiongetCli();
    this.peticionget();
    this.cargarDataVenta();
    this.cargarDataVenta2();
    
    Object.keys(this.state.producto).map(i => {
      
      this.state.idPreVenta=i;
      
      })
    
  }

  llenarCL() {
    
    this.state.formc.Nombre = this.state.cliente.Nombre
    this.state.formc.cedula = this.state.cliente.cedula
    this.state.formc.email = this.state.cliente.email
    this.state.formc.direccion = this.state.cliente.direccion
    this.state.formc.telefono = this.state.cliente.telefono
    this.setState({ modalEditar: false })
  }

  cargarDataVenta2() {
    console.log('carga venta');
    firebase.database().ref('Venta/').on('value', snapshot => {
      const data = snapshot.val();
      if (data != null) {
        this.setState({ vent: data });
      } else {
        this.setState({ nodata: true });

      }
    });

  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }


  seleccionarProducto = async (canal) => {
    await this.setState({ formcs: canal });
    this.setState({ modalEditar: true })

  }
  formatDate(date) {
    const options = { year: 'numeric', month: '2-digit', day: 'numeric' };
    return date.toLocaleDateString("zh-Hans-CN", options);
  }
  handleChange = e => {
    console.log(e.target.name);
    if (e.target.name !== 'cedula') {
      console.log("esto")
      this.setState({
        formc: {
          ...this.state.formc,
          [e.target.name]: e.target.value.toUpperCase()
        }
      })
      console.log(this.state.formc);

    } else {
      this.validar(e.target.value);
    }
  }
  peticionPut = () => {
    firebase.database().ref('Cliente/' + this.state.formc.cedula).set(this.state.formc,
      error => {
        this.setState({ data: this.state.formc, titleButton: 'Editar' });
        console.log(error)
      });
    this.setState({ modalEditar: false });

  }
  onSubmit = (e) => {
    e.preventDefault();
    const { formc } = this.state;
    const id = formc.id === '' 
    ?this.state.vent.length:formc.id;
     
    var newVenta = {
      id,
      Nombre: this.state.formc.Nombre.toUpperCase(),
      cedula: this.state.formc.cedula,
      email: this.state.formc.email,
      direccion: this.state.formc.direccion.toUpperCase(),
      telefono: this.state.formc.telefono,
      Fecha: this.formatDate(new Date()),
      producto: this.state.producto,
      Total: this.state.totalpag,
      Estado:'Realizado'
    }
    
    
    
    
    
    firebase.database().ref('VentaRes/' +  newVenta.id).set(newVenta);
    
    firebase.database().ref('ClientePed').remove();
    
    //se saca del objeto proveedor1
    firebase.database().ref('Venta/' + newVenta.id).set(newVenta);

    this.setState(prevState => ({
      open: true, setOpen: true, nodata: false, titleButton: 'Enviar',
      formc: {
        ...prevState.formc, id: '',
        ...prevState.formc, Nombre: '', cedula: '', email: '', direccion: '', telefono: '',
      }
    }))
    firebase.database().ref('Pedido/').remove();
    this.setState({ setOpen: true, open: true });
    window.location.href = <Home />;
  }
  cargarDataVenta() {
    console.log('carga venta');
    firebase.database().ref('VentaRes/').on('value', snapshot => {
      const data = snapshot.val();
      if (data != null) {
      this.setState({ ventaH: data });
        
        
      } else {
        this.setState({ nodata: true });

      }
    });
   

  }
  peticionget = () => {

    firebase.database().ref('Pedido').on('value', datos => {
       console.log(datos.val());
      if (datos.val() !== null) {
       // this.setState({ ...this.state.producto, producto: datos.val() })
       this.state.producto=datos.val(); 
       console.log(this.state.producto);
      } else {
        this.setState({ producto: [] })
      }
    })
  
    firebase.database().ref('Cliente').on('value', datos => {

      if (datos.val() !== null) {
        this.setState({ clienteP: datos.val() })
      } else {
        this.setState({ clienteP: [] })
      }
    })
  
  }


  peticiongetCli = () => {

    firebase.database().ref('ClientePed').on('value', datos => {
      
      if (datos.val() !== null) {
       // this.setState({ ...this.state.producto, producto: datos.val() })
       this.state.cliente=datos.val(); 
       
      } else {
        this.setState({ cliente: [] })
      }
    })
  
    this.llenarCL();
  }

  total() {
    console.log(this.ccyFormat((this.TAX_RATE * this.invoiceSubtotal) + this.invoiceSubtotal))
    var tot = this.ccyFormat((this.TAX_RATE * this.invoiceSubtotal) + this.invoiceSubtotal);
    this.state.totalpag = tot;
    console.log(this.state.totalpag);
    return tot
  }
  handleDelete() {

  }

  imprimir() {
    console.log("llego")
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    mywindow.document.write('<html><head>');
    mywindow.document.write('<style>.tablaN{display:none;}.tablaF{width:25%;border-collapse:collapse;margin:40px 2px 1px 25px;}.tablaF th{border:0px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;color:transparent;} .tablaC{width:25%;border-collapse:collapse;margin:0 2px 0 25px;}.tablaC th{border:0px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;color:transparent;}.tablaC td{border:0px solid #ddd;text-align:center;}          .tabla{width:25%;border-collapse:collapse;margin:80px 2px 0 15px;}.tabla th{border:0px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;color:transparent;}.tabla td{border:0px solid #ddd;text-align:left;padding:2px;}.tabla tr{border:0px solid #ddd;text-align:left;padding:1px;}</style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById("tabla").innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necesario para IE >= 10
    mywindow.focus(); // necesario para IE >= 10
    mywindow.print();
    mywindow.close();

  }



  filter(event) {

    var text = event.target.value.toUpperCase();
    var data = this.state.clienteP
    data = Object.values(data)
    console.log(this.state.clienteP)
    const newData = data.filter(function (item) {
      const itemcedula = item.cedula
      const itemnombre = item.Nombre.toUpperCase()
      const campo = itemcedula + " " + itemnombre;
      const textData = text.toUpperCase()
      return campo.indexOf(textData) > -1

    })
    this.setState({
      clienteP: newData,

    })

  }

  llenar(i) {
    console.log(i)

    this.state.formc.Nombre = this.state.clienteP[i].Nombre
    this.state.formc.cedula = this.state.clienteP[i].cedula
    this.state.formc.email = this.state.clienteP[i].email
    this.state.formc.direccion = this.state.clienteP[i].direccion
    this.state.formc.telefono = this.state.clienteP[i].telefono
    this.setState({ modalEditar: false })
  }
  ccyFormat(num) {
    return `${num.toFixed(2)}`;
  }


  render() {
    const theme = createMuiTheme({
      palette: {
        primary: {
          light: '#6103F0',
          main: '#651fff',
          dark: '#6103F0',

        },
        secondary: {
          light: '#ff7961',
          main: '#651fff',
          dark: '#ba000d',
          contrastText: '#000',
        },
      },
    });


    const StyledTableCell = withStyles((theme) =>
      createStyles({

        head: {
          backgroundColor: theme.palette.primary.main,
          color: theme.palette.common.white,
          fontSize: 14,
          right: 3,
          width: '1%',
        },
        body: {
          fontSize: 11,
          right: 3,
          width: '1%',


        },

        root: {
          width: '0.3%',
          margin: theme.spacing(0.2),
        },

      }),
    )(TableCell);

    const StyledTableRow = withStyles((theme) =>
      createStyles({
        root: {
          '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,

          },
        },
      }),
    )(TableRow);


    function ccyFormat(num) {
      return `${num.toFixed(2)}`;
    }
    var subtotalp = 0;
    var ivasum = 0;

    function suma(val) {

      subtotalp = subtotalp + val;
      return subtotalp;
    }
    function Iva(val) {

      ivasum = ivasum + val;
      return ivasum;
    }


    function priceRow(qty, unit) {
      return qty * unit;
    }

    function createRow(desc, qty, unit) {
      const price = priceRow(qty, unit);
      return { desc, qty, unit, price };
    }

    function subtotal(items) {

      return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
    }


    var TAX_RATE = 0.12;





    var invoiceSubtotal = 0;
    const invoiceTaxes = 0;
    const invoiceTotal = 0;
    const formatDate = (date) => {

      const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      return date.toLocaleDateString(undefined, options);


    }
    return (
      <>
        <div>

          <button onClick={() => this.imprimir()} className="botonesfactura">Imprimir</button>
          <button onClick={() => this.seleccionarProducto(this.state.formc, 4, 'Editar')} className="botonesfactura" >Editar Cliente</button>
          <button onClick={this.onSubmit} className="botonesfactura" >Finalizar</button>

        </div>
<div className="Fact">
        <TableContainer id="tabla">

          <Table   className="tabla">
            <br></br>
            <tr >
              <th className="rowt2" >Fecha:</th>
              <td className="rowt4" style={{ whiteSpace: 'pre' }}>{formatDate(this.state.date)}</td>
            </tr>
            <tr>
              <th className="rowt2" >Cliente</th>
              <td className="rowt4" style={{ whiteSpace: 'pre' }}>{this.state.formc.Nombre}</td>
            </tr>
            <tr>
              <th className="rowt2" >Dirección</th>
              <td className="rowt4" style={{ whiteSpace: 'pre' }}>{this.state.formc.direccion}</td>
            </tr>
            <tr>
              <th className="rowt2"  >R.U.C.</th>
              <td className="rowt4" style={{ whiteSpace: 'pre' }}>{this.state.formc.cedula}</td>
            </tr>
          </Table>
      

        <TableContainer id="tablaC" className="tablaC">
          <Table   className="tablaC">
            <TableRow >
              <th >Cant</th>
              <th >Descripcion</th>
              <th >V.Unit</th>
              <th >V.Total</th>
              <th style={{ display: 'none' }} >Sumatoria</th>
            </TableRow>
            {!this.props.NoData
              ? Object.keys(this.state.producto).map(i => {
                return (
                  <TableRow hover key={i} >
                    <TableCell  colspan="1" align="center">{this.state.producto[i].Cantidad}</TableCell>
                    <TableCell  colspan="4" align="center" style={{ fontSize: '0.7em'}} >{this.state.producto[i].NombrePro + this.state.producto[i].Detalle}</TableCell>
                    <TableCell  colspan="3" align="center">{ ccyFormat(this.state.producto[i].CostoVen)}</TableCell>
                    <TableCell   colspan="4" align="center" style={{ paddingLeft:'1em' }} >  {ccyFormat(this.state.producto[i].Cantidad * this.state.producto[i].CostoVen)}</TableCell>
                    <TableCell  className="suma" style={{ display: 'none' }}  >{
                      ccyFormat(
                        invoiceSubtotal = suma((this.state.producto[i].Cantidad * this.state.producto[i].CostoVen)), this.state.iva = Iva(Number(this.state.producto[i].Cantidad * this.state.producto[i].CostoVen) * Number(this.state.producto[i].Iva + 1)))}
                    </TableCell >
                  </TableRow>
                )
              })
              : 'sin datos'}
          </Table>
        </TableContainer>
       <TableContainer id="tablaN">
          <Table className="tablaN">
            <TableRow >
              <th  colspan="1">Cant</th>
              <th  colspan="2">Descripcion</th>
              <th  colspan="1">V.Unit</th>
              <th  colspan="1">V.Total</th>
              <th style={{ display: 'none' }} >Sumatoria</th>
            </TableRow>
            {!this.props.NoData
              ? Object.keys(this.state.producto).map(i => {
                return (
                  <TableRow hover key={i} >
                    <TableCell colspan="1" align="center">{this.state.producto[i].Cantidad}</TableCell>
                    <TableCell  colspan="2" align="center" style={{ fontSize: '0.7em'}} >{this.state.producto[i].NombrePro + this.state.producto[i].Detalle}</TableCell>
                    <TableCell  colspan="1" align="center">{ ccyFormat(this.state.producto[i].CostoVen)}</TableCell>
                    <TableCell   colspan="1" align="center" style={{ paddingLeft:'1em' }} >  {ccyFormat(this.state.producto[i].Cantidad * this.state.producto[i].CostoVen)}</TableCell>
                    <TableCell  className="suma" style={{ display: 'none' }}  >{
                      ccyFormat(
                        invoiceSubtotal = suma((this.state.producto[i].Cantidad * this.state.producto[i].CostoVen)), this.state.iva = Iva(Number(this.state.producto[i].Cantidad * this.state.producto[i].CostoVen) * Number(this.state.producto[i].Iva + 1)))}
                    </TableCell >
                  </TableRow>
                )
              })
              : 'sin datos'}
          </Table>
          </TableContainer>



<TableContainer id="tablaF" style={{marginLeft:184}}>
        <Table className="tablaF" >
          <TableRow >
            <TableCell align="right" style={{fontWeight: 'bold'}} >Subtotal</TableCell>
            <TableCell align="left" style={{ paddingLeft:'1em' }}>{ccyFormat(invoiceSubtotal)}</TableCell>
            <br></br>
          </TableRow>
          <TableRow>
            <TableCell align="right" style={{marginRight:150,fontWeight: 'bold'}} >IVA 12%</TableCell>
            <TableCell align="left"style={{ paddingLeft:'1em' }}>{ccyFormat(this.state.iva - invoiceSubtotal)}</TableCell>
          </TableRow>
          <br></br>

          <TableRow>
            <th align="right" style={{fontWeight: 'bold'}}>Total</th>
            <TableCell align="left" style={{ paddingLeft:'1em' }}>{this.state.totalpag = ccyFormat((this.state.iva - invoiceSubtotal) + invoiceSubtotal)}</TableCell>
          </TableRow>

        </Table>
        </TableContainer>
        </TableContainer >

        </div>




        <Modal toggle={this.toggle} isOpen={this.state.modalEditar} className="Modal">

          <Container  >
            <SearchTwoToneIcon className="iconoBusqueda2" color="primary" />
            <ThreeSixtyIcon onClick={() => this.peticionget()} className="iconoreload" color="primary" />
            <input class="busqueda2"
              value={this.state.text}
              onChange={(text) => this.filter(text)} />




            <ModalHeader> </ModalHeader>
            <Typography color="primary" className="titulos" variant="h6" gutterBottom>Agregar Clientes </Typography>
            < AssignmentIndIcon onClick={() => this.setState({ modalCli: true })} color="primary" className="aggIc" style={{ fontSize: 40 }} />
            <ModalBody>
              <Typography color="primary" className="titulos" variant="h6" gutterBottom>Listado Clientes </Typography>
              <TableContainer className="contenedorTabla" id="tabla">
                <Table className="tabla" >
                  <TableHead >
                    <StyledTableRow>
                      <StyledTableCell >Cedula</StyledTableCell>
                      <StyledTableCell >Nombre</StyledTableCell>
                      <StyledTableCell >Accion</StyledTableCell>
                    </StyledTableRow>
                  </TableHead>
                  <TableBody className="TableBody">


                    {!this.props.NoData
                      ? Object.keys(this.state.clienteP).map(i => {
                        return (
                          <TableRow hover key={i}>
                            <StyledTableCell >{this.state.clienteP[i].cedula}</StyledTableCell>
                            <StyledTableCell >{this.state.clienteP[i].Nombre}</StyledTableCell>
                            <Tooltip title="Aceptar" ><StyledTableCell > <BeenhereIcon color="primary" onClick={() => this.llenar(i)} /> </StyledTableCell></Tooltip>
                          </TableRow>

                        )
                      })
                      : 'sin datos'}

                  </TableBody>
                </Table>
              </TableContainer >
            </ModalBody>
            <ModalFooter className="ModalFooter">
              <br></br><br></br>
              <Button variant="contained" color="primary" onClick={() => this.peticionPut()}>Enviar</Button>{"   "}
              <Button variant="contained" color="primary" onClick={() => this.setState({ modalEditar: false })}>Cancelar</Button>

            </ModalFooter>
          </Container>
        </Modal>
        <Modal toggle={this.toggle} isOpen={this.state.modalCli} className="ModalCli">
          <Container  >
            <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom> Formulario de Venta </Typography></ModalHeader>
            <br></br>
            <ModalBody>
              <Grid container spacing={2} >
                <Grid className="grid">
                  <form onSubmit={this.onSubmit}>
                    <Grid container spacing={2} >
                      <TextField onChange={this.handleChange} required
                        id="Nombre"
                        name="Nombre"
                        label="Nombre"
                        fullWidth
                        value={this.state.Nombre} />
                      <Grid item xs={12} sm={6}>
                        <TextField onChange={this.handleChange} required
                          id="cedula"
                          name="cedula"
                          type="text"
                          label="cedula"
                          inputProps={{
                            maxLength: 10,
                          }}
                          fullWidth
                          value={this.state.cedula} />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <TextField onChange={this.handleChange} required
                          id="email"
                          name="email"
                          label="email"
                          type="email"
                          fullWidth
                          value={this.state.email} />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <TextField onChange={this.handleChange} required
                          id="direccion"
                          name="direccion"
                          type="text"
                          label="direccion"
                          fullWidth
                          value={this.state.direccion} />
                      </Grid>
                      <Grid item xs={12} sm={8} >
                        <TextField onChange={this.handleChange} required
                          id="telefono"
                          name="telefono"
                          type="number"
                          label="telefono"
                          fullWidth
                          value={this.state.telefono} />
                      </Grid>
                    </Grid>
                  </form>
                </Grid>
              </Grid>
            </ModalBody>
            <ModalFooter className="ModalFooter">
              <br></br>
              <Button variant="contained" color="primary" onClick={() => this.peticionPut()}>Aceptar</Button>{"   "}
              <Button variant="contained" color="primary" onClick={() => this.setState({ modalCli: false })}>Cerrar</Button>
            </ModalFooter>
          </Container>
        </Modal>
      </>
    );
  }
} export default factura;
