import React, { Fragment, useState, useCallback, Component } from 'react';
import Case from '../case';
//import AgP from './AgregaProv';
//import LsP from './ListarProv';
import ag from './imagenes/agregarV.png';
import li from './imagenes/Listar.png';
class Proveedor extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.state = {
            val: ''
        }
        
    }
    handleChange(val) {
console.log(val)
        this.setState({ val: val });

    }
    render() {

        return (
            <>
                <img onClick={() => this.handleChange("AgregaProv")} id="icono" src={ag} className="imgv"></img>
                <img onClick={() => this.handleChange("ListarProv")} id="icono" src={li} className="imgv"></img>
                <Case status={this.state.val} />
            </>
        )
    }
}
export default Proveedor;