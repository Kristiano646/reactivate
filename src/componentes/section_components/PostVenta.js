import React, { Fragment, useState, useCallback, Component } from 'react';
import ReactDOM from 'react-dom';
import { makeStyles, withStyles, createStyles, createMuiTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import { icon } from '@fortawesome/fontawesome-svg-core';
import li from './imagenes/+.png';

import Case from '../case';
import { ContactSupportOutlined } from '@material-ui/icons';
class Proveedor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ven: [],
            val: '',
            data: [],
            modalVender: false,
            term: 'sin datos',
            producto: [],
            form: {
                id: '',
                NombrePro: '',
                CostoProv: '',
                CostoVen: '',
                Stock: 0,
                StockMin: 0,
                Tipo: '',
                Detalle: '',
                Marca: '',
                Fecha: '',
                Iva:''
            },
            id: '',
            cantidadp: {
                cantidad: 0,

            },

            venta: [],


        };
    }
    toggle = this.toggle.bind(this);
    cargarData = this.cargarData.bind(this);
    toggle() {
        this.setState({
            modal: !this.state.modal
        });

    }
    agregarventa = async (canal) => {
        console.log(canal)
        await this.setState({ cantidad: canal });
        /*this.setState({ modalVender: true })*/
        var newPedido = {
            NombrePro: this.state.ven.NombrePro.toUpperCase(),
            CostoVen: this.state.ven.CostoVen,
            Stock: this.state.ven.Stock,
            Detalle: this.state.ven.Detalle.toUpperCase(),
            Fecha: this.state.ven.Fecha,
            id: this.state.ven.id,
            tipo: this.state.ven.Tipo,
            Cantidad: this.state.cantidadp.cantidad,
          

        }
        firebase.database().ref('Venta/' + newPedido.id).set(newPedido);

    }
    peticionget = () => {
        firebase.database().ref().child('Producto').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.data, data: datos.val() })
                    this.setState({ ...this.state.producto, producto: datos.val() })
                } else {
                    this.setState({ data: [] })
                }
            })
    }

    filter(event) {
        var text = event.target.value.toUpperCase();
        console.log(this.state.data);
        const data = this.state.data
        const newData = data.filter(function (item) {
            const itemid = item.id
            const itemDataTitle = item.NombrePro.toUpperCase()
            const itemDataDescp = item.Detalle.toUpperCase()
            const itemDataTipo = item.Tipo.toUpperCase()
            const itemDataMarca = item.Marca.toUpperCase()
            const campo = itemid + " " + itemDataTitle + " " + itemDataDescp + " " + itemDataTipo + " " + itemDataMarca + " ";
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            producto: newData,

        })
    }



    peticionDelete = () => {
        if (
            swal({
                title: "¿Estás seguro que deseas eliminar el Producto",
                text: " " + this.state.form.NombrePro + "?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        firebase.database().ref('Pedido/' + this.state.id).remove()
                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino el producto!");
                    }
                }
                )
        );
    }



    peticiongetPedido = () => {

        firebase.database().ref().child('Pedido').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.ven, ven: datos.val() })

                } else {
                    this.setState({ data: [] })
                }
            })

    }
    componentWillMount() {
        this.peticionget();
        this.cargarData();
        

    }


    mostrarPedido() {
        this.peticiongetPedido();
        this.setState({ modalVender: true });
        this.cargarData();
    }
    seleccionarProducto2 = async (canal, id, caso) => {
        await this.setState({ form: canal, id: id });
        (caso === "Editar")
            ? this.setState({ modalEditar: true })
            : this.peticionDelete()
    }
    seleccionarProducto = async (canal, id, caso) => {

        console.log(canal)
        await this.setState({ form: canal, id: id });
        if (caso === "Vender") {

            this.setState({ modalVender: true })
            var newPedido = {
                NombrePro: this.state.form.NombrePro.toUpperCase(),
                CostoVen: this.state.form.CostoVen,
                Stock: this.state.form.Stock - this.state.cantidadp.cantidad,
                Detalle: this.state.form.Detalle.toUpperCase(),
                Fecha: this.state.form.Fecha,
                id: this.state.form.id,
                Tipo: this.state.form.Tipo,
                Cantidad: this.state.cantidadp.cantidad,
                Iva:this.state.form.Iva
            }
            firebase.database().ref('Pedido/' + newPedido.id).set(newPedido);
            this.state.form.Stock = newPedido.Stock;
            firebase.database().ref('Producto/' + this.state.form.id).set(this.state.form);

            this.peticiongetPedido();
        } else {
            this.cancelar();
        }
    }

    cancelar = async (canal) => {
        if (
            await swal({
                title: "¿Estás seguro que deseas eliminar el Producto",
                text: " " + this.state.form.NombrePro + "del pedido?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        let cant = 0;
                        cant = this.state.cantidadp.cantidad;
                        let sto = 0;
                        sto = this.state.form.Stock;
                        let stk = (Number(sto) + Number(cant));
                        this.state.form.Stock = stk;
                        firebase.database().ref('Producto/' + this.state.form.id).set(this.state.form);
                        firebase.database().ref('Pedido/' + this.state.id).remove();
                        this.setState({ modalVender: false })

                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino el producto!");
                    }
                }
                )
        );
    }

    cantidad = e => {
        console.log(this.state.producto[e.target.id].Stock);
       if(this.state.producto[e.target.id].Stock < e.target.value){
       
        swal({
            title: "Stock Insuficiente",
            text: " " + this.state.producto[e.target.id].NombrePro + " sin Stock....!!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        e.target.value='';
        
       }else{
       
        this.state.cantidadp.cantidad = e.target.value
       }
    }
    cargarData() {
        firebase.database().ref('Pedido/').on('value', datos => {
            if (datos.val() !== null) {
                this.setState({ ...this.state.venta, venta: datos.val() })

            } else {
                this.setState({ venta: [] })
            }
        });

    }

    enviar = () => {



        document.getElementById('Ventas').style.display = 'none';
        document.getElementById('Factura').style.display = 'inline';
        this.setState({ modalVender: false });


    }



    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',
                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });
        const StyledTableCell = withStyles((theme) =>
            createStyles({
                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 14,
                },
                body: {
                    fontSize: 11,
                },
                root: {
                    width: '15%',
                },
            }),
        )(TableCell);
        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,
                    },
                },
            }),
        )(TableRow);
        function ccyFormat(num) {
            return `${num.toFixed(2)}`;
          }

        return (
            <>
                <div id="Ventas">
                    <SearchTwoToneIcon className="iconoBusqueda" color="primary" />
                    <input class="busqueda"
                        value={this.state.text}
                        onChange={(text) => this.filter(text)} />
                    <br></br>
                    <Button variant="contained" color="primary" onClick={() => this.mostrarPedido()}>Pedido</Button>
                    <br></br>
                    <br></br>
                    <TableContainer className="contenedorTabla">
                        <Table className="tabla">
                            <TableHead >
                                <TableRow >
                                    <StyledTableCell >Codigo</StyledTableCell>
                                    <StyledTableCell >Producto</StyledTableCell>
                                    <StyledTableCell >Costo Compra</StyledTableCell>
                                    <StyledTableCell >Costo Venta</StyledTableCell>
                                    <StyledTableCell >Cantidad</StyledTableCell>
                                    <StyledTableCell >Stock</StyledTableCell>
                                    <StyledTableCell >Tipo</StyledTableCell>
                                    <StyledTableCell >Detalle</StyledTableCell>
                                    <StyledTableCell >Añadir</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody className="TableBody">
                                {!this.props.NoData
                                    ? Object.keys(this.state.producto).map(i => {
                                        return (
                                            <StyledTableRow hover key={i}>
                                                <StyledTableCell align="left">{this.state.producto[i].id}</StyledTableCell>
                                                <StyledTableCell align="left">{this.state.producto[i].NombrePro}</StyledTableCell>
                                                <StyledTableCell align="left">{ccyFormat(this.state.producto[i].CostoProv)}</StyledTableCell>
                                                <StyledTableCell align="left">{ccyFormat(this.state.producto[i].CostoVen)}</StyledTableCell>
                                                <StyledTableCell >
                                                    <TextField onChange={this.cantidad}
                                                        id={i}
                                                        name="Cantidad"
                                                        label="Cantidad"
                                                        type="number"
                                                        InputProps={{ inputProps: { min: 0, max: 1000 } }}
                                                        fullWidth
                                                    />

                                                </StyledTableCell>
                                                <StyledTableCell align="left">{this.state.producto[i].Stock}</StyledTableCell>
                                                <StyledTableCell align="left">{this.state.producto[i].Tipo}</StyledTableCell>
                                                <StyledTableCell align="left">{this.state.producto[i].Detalle}</StyledTableCell>
                                                <StyledTableCell align="left" style={{ display: 'none' }}>{this.state.producto[i].Iva }</StyledTableCell>
                                                <StyledTableCell align="left">
                                                    <Icon color="primary"><img onClick={() => this.seleccionarProducto(this.state.producto[i], i, 'Vender')} id="icono" src={li} className="mas"></img></Icon>
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        )
                                    })
                                    : 'sin datos'}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Modal toggle={this.toggle} isOpen={this.state.modalVender} className="Modal2" cargarData={this.cargarData}>
                        <Container  >
                            <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom> Formulario de Venta </Typography></ModalHeader>
                            <br></br>
                            <ModalBody>
                                <Grid container spacing={2} >
                                    <Grid className="grid">
                                    </Grid>
                                </Grid>
                            </ModalBody>
                            <ModalFooter className="ModalFooter">
                                <TableContainer className="contenedorTabla">
                                    <Table className="tabla">
                                        <TableHead >
                                            <TableRow >
                                                <StyledTableCell >Producto</StyledTableCell>
                                                <StyledTableCell >PVP</StyledTableCell>
                                                <StyledTableCell >Cantidad</StyledTableCell>
                                                <StyledTableCell >Tipo</StyledTableCell>
                                                <StyledTableCell >Detalle</StyledTableCell>
                                                <StyledTableCell >Accion</StyledTableCell>

                                            </TableRow>
                                        </TableHead>
                                        <TableBody className="TableBody">
                                            {!this.props.NoData
                                                ? Object.keys(this.state.ven).map(i => {
                                                    return (
                                                        <StyledTableRow hover key={i}>
                                                            <StyledTableCell align="left">{this.state.ven[i].NombrePro}</StyledTableCell>
                                                            <StyledTableCell align="left">{ccyFormat(this.state.ven[i].CostoVen)}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Cantidad}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Tipo}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Detalle}</StyledTableCell>
                                                            <StyledTableCell align="left">
                                                                <DeleteRoundedIcon fontSize="small" color="primary" onClick={() => this.seleccionarProducto(this.state.producto[i], i, 'Eliminar')} />
                                                            </StyledTableCell>
                                                        </StyledTableRow>
                                                    )
                                                })
                                                : 'sin datos'}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <br></br>
                                <Button variant="contained" color="primary" onClick={() => this.enviar()}>Facturar</Button>{"   "}
                                <Button variant="contained" color="primary" onClick={() => this.setState({ modalVender: false })}>Adicionar</Button>







                            </ModalFooter>
                        </Container>
                    </Modal>
                </div>
                <div id="Factura" className="Factura">
                    <Case status="Factura" />
                </div>






            </>
        )
    }
}
export default Proveedor;
