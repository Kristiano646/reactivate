import { React, Fragment, Component } from 'react';
import app from '../../util/firebase';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import firebase from 'firebase/app';
import 'firebase/database';
import { createMuiTheme } from '@material-ui/core/styles';
import swal from 'sweetalert';
import 'firebase/auth';
import firebaseConfig from '../../util/firebase';


class Vendedor extends Component {
    theme = createMuiTheme({
        palette: {
            primary: {
                light: '#757ce8',
                main: '#3f50b5',
                dark: '#002884',
                contrastText: '#fff',
            },
            secondary: {
                light: '#ff7961',
                main: '#f44336',
                dark: '#ba000d',
                contrastText: '#000',
            },
        },
    });
    // Json este es el objeto
    state = {
        vendedor:
        {
            id: '',
            NombreVend: '',
            ApellidoVend: '',
            CedulaVend: '',
            TelefonoVend: '',
            CorreoVend: '',
            Contraseña:'',
            status: false
        },
        vendedor1: [],
        open: false,
        setOpen: false,
        nodata: false,
        titleButton: 'Guardar'
    }

    componentDidMount() {
        this.cargarDataV();
    }

    cargarDataV() {
        firebase.database().ref('Vendedor/').on('value', snapshot => {
            const data = snapshot.val();
            if (data != null) {
                this.setState({ vendedor1: data });
            } else {
                this.setState({ nodata: true });
            }
        });
    }
    onSubmit = (e) => {
        e.preventDefault();
        const { vendedor } = this.state;
        const id = vendedor.id === '' ?
            this.state.vendedor1.length : vendedor.id;
        var newVendedor = {

            NombreVend: vendedor.NombreVend.toUpperCase(),
            ApellidoVend: vendedor.ApellidoVend.toUpperCase(),
            CedulaVend: vendedor.CedulaVend.toUpperCase(),
            TelefonoVend: vendedor.TelefonoVend.toUpperCase(),
            Contraseña:vendedor.Contraseña,
            CorreoVend: vendedor.CorreoVend.toUpperCase(),
            id,
            status: vendedor.status

        }

        //se saca del objeto vendedor1
        firebase.database().ref('Vendedor/' + newVendedor.id).set(newVendedor);
        firebaseConfig.auth().createUserWithEmailAndPassword(vendedor.CorreoVend,vendedor.Contraseña);
        

        this.setState(prevState => ({
            open: true, setOpen: true, nodata: false,
            vendedor: {
                ...prevState.vendedor, id: '',
                ...prevState.vendedor, NombreVend: '', ApellidoVend: '',CedulaVend: '',
               TelefonoVend: '', CorreoVend: '',Contraseña:''
            }
        }))
        console.log(this.state.vendedor.CedulaVend)
    }


    handleDelete = (id) => {
        firebase.database().ref('Vendedor/' + id).remove();
        this.setState({ setOpen: true, open: true });
    }

    handleEdit = (id) => {
        firebase.database().ref('Vendedor/' + id).on('value', snapshot => {
            const data = snapshot.val();
            this.setState({ vendedor: data.vendedor, titleButton: 'Editar' });
        });


    }
    onChange = e => {
        console.log(e.target.name);
        if (e.target.name !== 'cedula') {
          console.log("esto")
          this.setState({
            vendedor: {
              ...this.state.vendedor,
              [e.target.name]: e.target.value.toUpperCase()
            }
          })
          console.log(this.state.vendedor);
    
        } else {
          this.validar(e.target.value);
        }

    }
    validar(cedulaval) {
        console.log(cedulaval)
        var cad = cedulaval
        var total = 0;
        var longitud = cad.length;
        var longcheck = longitud - 1;

        if (cad !== "" && longitud === 10) {
            for (var i = 0; i < longcheck; i++) {
                if (i % 2 === 0) {
                    var aux = cad.charAt(i) * 2;
                    if (aux > 9) aux -= 9;
                    total += aux;
                } else {
                    total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
                }
            }

            total = total % 10 ? 10 - total % 10 : 0;

            if (cad.charAt(longitud - 1) == total) {
                console.log("vale")
                this.setState({
                    vendedor: {
                        ...this.state.vendedor,
                        ...this.state.vendedor, CedulaVend: cedulaval
                    }
                })

            } else {
                console.log("no vale")
                swal({
                    title: "Numero de Cédula no Valido",
                    text: "Ingreselo nuevamente ",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
            }
        }
    }

    handleClose = () => {
        this.setState({
            open: false, setOpen: false
        });
    }
    render() {
        return (

            <Container id="contenedorForm" className="contenedorForm" >
                <Grid container spacing={2} >
                    <Grid className="grid">
                        <Typography color="primary" className="titulos" variant="h6" gutterBottom>Ingresar Vendedor </Typography>
                        <form onSubmit={this.onSubmit}>
                            <Grid container spacing={2} >
                                <Grid item xs={12} sm={6}>
                                    <TextField onChange={this.onChange} required
                                        id="NombreVend"
                                        name="NombreVend"
                                        label="Nombre Vendedor"
                                        fullWidth
                                        value={this.state.vendedor.NombreVend} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField onChange={this.onChange} required
                                        id="ApellidoVend"
                                        name="ApellidoVend"
                                        label="Apellido Vendedor"
                                        fullWidth
                                        value={this.state.vendedor.ApellidoVend} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField onChange={this.onChange} required
                                        id="cedula"
                                        name="cedula"
                                        type="text"
                                        label="cedula"
                                        inputProps={{
                                            maxLength: 10,
                                        }}
                                        fullWidth
                                        value={this.state.cedula} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField onChange={this.onChange} required
                                        id="TelefonoVend"
                                        name="TelefonoVend"
                                        label="Teléfono Vendedor"
                                        fullWidth
                                        value={this.state.vendedor.TelefonoVend} />
                                </Grid>

                                <Grid item xs={12} sm={8} >
                                    <TextField onChange={this.onChange} required
                                        id="CorreoVend"
                                        name="CorreoVend"
                                        label="Correo Vendedor"
                                        type="email"
                                        fullWidth
                                        value={this.state.vendedor.CorreoVend} />
                                </Grid>

                                <Grid item xs={12} sm={8} >
                                    <TextField onChange={this.onChange} required
                                        id="Contraseña"
                                        name="Contraseña"
                                        label="Contraseña"
                                        type="password"
                                        fullWidth
                                        value={this.state.vendedor.Contraseña} />
                                </Grid>



                                
                                <Grid item xs={12} sm={8}>
                                    <Button variant="contained" color="primary" type="submit"> {this.state.titleButton} </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Grid>
                    <Dialog open={this.state.open} onClose={this.handleClose} >
                        <DialogTitle className="dialogo">{"Se almacenó satisfactoriamente."}</DialogTitle>
                        <DialogActions className="dialogo">
                            <Button onClick={this.handleClose} color={this.theme.palette.secondary.light}>
                                Cerrar
                        </Button>
                        </DialogActions>
                    </Dialog>
                </Grid>
            </Container>
        )
    };
}
export default Vendedor;