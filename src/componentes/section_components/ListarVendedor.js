import { React, Component } from 'react';
import { withStyles, createStyles, createMuiTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import 'firebase/auth';
import firebaseConfig from '../../util/firebase';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
const eye = <FontAwesomeIcon icon={faEye} />;
class ListarV extends Component {
    constructor(props) {
        super(props);
       this.state = {
        data: [],
        modalEditar: false,
        form: {
            NombreVend: ''.toUpperCase(),
            ApellidoVend: ''.toUpperCase(),
            CedulaVend: '',
            TelefonoVend: '',
            CorreoVend: '',
            status: false
        },
        id: 0,
        passwordShown:false,  
    }
    this.toggle = this.toggle.bind(this);
    this.passwordShown=this.togglePasswordVisiblity.bind(this);   
}
   

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    togglePasswordVisiblity() {
  
        this.setState({
            passwordShown:!this.state.passwordShown
            ?true
            :false
        })
       console.log(this.state.passwordShown) 

    }
    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value.toUpperCase()
            }
        })
        console.log(this.state.form);
    }
    peticionget = () => {
        firebase.database().ref().child('Vendedor').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.data, data: datos.val() })
                } else {
                    this.setState({ data: [] })
                }
            })
    }
   componentWillMount() {
        this.peticionget();
    }
    /*  peticionPost = () => {
          firebase.database().ref().child('Producto').child(this.state.id).child('producto').
              push(this.state.data, error => {
                  if (error) console.log(error)
              });
          this.setState({ modalInsertar: false })
      }*/

    seleccionarProducto = async (canal, id, caso) => {
        await this.setState({ form: canal, id: id });
        (caso === "Editar") 
        ? this.setState({ modalEditar: true })
         :this.peticionDelete()
    }
    peticionPut = () => {
        firebase.database().ref('Vendedor/' + this.state.id)
            .set(this.state.form,
                error => {
                    this.setState({ data: this.state.form, titleButton: 'Editar' });
                    console.log(error)
                });
        this.setState({ modalEditar: false });
        window.location.reload()
    }
    peticionDelete = () => {
        if (
            swal({
                title: "¿Estás seguro que deseas eliminar al Vendedor",
                text: " " + this.state.form.NombreVend + "?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        console.log(this.state.form.CorreoVend)
                        firebaseConfig.auth().signInWithEmailAndPassword(this.state.form.CorreoVend,this.state.form.Contraseña);
                        firebaseConfig.auth().currentUser.delete();
                        firebase.database().ref('Vendedor/' + this.state.id).remove(); 
                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino al Vendedor!");
                    }
                }
                )
        );
    }


    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',

                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });


        const StyledTableCell = withStyles((theme) =>
            createStyles({

                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 13,
                    
                },
                body: {
                    fontSize: 11,
                    
                     
                },
                
                root: {
                    width: '12%',
                   
                  },
                 
            }),
        )(TableCell);

        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,
                    },
                },
            }),
        )(TableRow);
return (
            <>
                <TableContainer className="contenedorTabla">
                    <Table className="tabla">
                        <TableHead >
                      
                            <TableRow >
                                <StyledTableCell >ID</StyledTableCell>
                                <StyledTableCell >Nombre</StyledTableCell>
                                <StyledTableCell >Apellido</StyledTableCell>
                                <StyledTableCell >Cédula</StyledTableCell>
                                <StyledTableCell >Celular</StyledTableCell>
                                <StyledTableCell >Contraseña<i className="ojo" onClick={() => this.togglePasswordVisiblity() }>{eye}</i> </StyledTableCell>
                                <StyledTableCell  >Correo</StyledTableCell>
                                <StyledTableCell >Acciones</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody className="TableBody">
                            {!this.props.NoData
                                ? Object.keys(this.state.data).map(i => {
                                    return (
                                        <StyledTableRow hover key={i}>
                                        <StyledTableCell align="left">{this.state.data[i].id}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data[i].NombreVend}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data[i].ApellidoVend}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data[i].CedulaVend}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data[i].TelefonoVend}</StyledTableCell>
                                            <TextField readonly type={this.state.passwordShown ? "text" : "password"}  value={this.state.data[i].Contraseña}></TextField>
                                            <StyledTableCell >{this.state.data[i].CorreoVend}</StyledTableCell>
                                         
                                              
                                        <StyledTableCell align="left">
                                                <BorderColorRoundedIcon fontSize="small"  onClick={() => this.seleccionarProducto(this.state.data[i], i, 'Editar')} color="primary" />
                                                <DeleteRoundedIcon fontSize="small" color="primary" onClick={() => this.seleccionarProducto(this.state.data[i], i, 'Eliminar')} />
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    )
                                })
                                : 'sin datos'}
                        </TableBody>
                    </Table>
                </TableContainer>
              <Modal toggle={this.toggle} isOpen={this.state.modalEditar} className="Modal">
                    <Container  >
                        <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom>Editar Producto </Typography></ModalHeader>
                        <ModalBody>
                            <Grid container spacing={2} >
                                <Grid className="grid">
                                    <form onSubmit={this.onSubmit}>
                                        <Grid container spacing={2} >
                                            <TextField onChange={this.handleChange} required
                                                id="NombreVend"
                                                name="NombreVend"
                                                label="Nombre "
                                                fullWidth
                                                value={this.state.form.NombreVend} />
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="ApellidoVend"
                                                    name="ApellidoVend"
                                                    label="Apellido"
                                                    fullWidth
                                                    value={this.state.form.ApellidoVend} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="CedulaVend"
                                                    name="CedulaVend"
                                                    label="Cédula"
                                                    type="number"
                                                    fullWidth
                                                    value={this.state.form.CedulaVend} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="TelefonoVend"
                                                    name="TelefonoVend"
                                                    type="number"
                                                    label="Celular"
                                                    fullWidth
                                                    value={this.state.form.TelefonoVend} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                            <TextField onChange={this.handleChange} required
                                                    id="CorreoVend"
                                                    name="CorreoVend"
                                                    type="email"
                                                    label="Correo"
                                                    fullWidth
                                                    value={this.state.form.CorreoVend} />
                                            </Grid>
                                        </Grid>
                                    </form>
                                </Grid>
                            </Grid>
                        </ModalBody>
                        <ModalFooter className="ModalFooter">
                            <br></br><br></br>
                            <Button variant="contained" color="primary" onClick={() => this.peticionPut()}>Enviar</Button>{"   "}
                            <Button variant="contained" color="primary" onClick={() => this.setState({ modalEditar: false })}>Cancelar</Button>

                        </ModalFooter>
                    </Container>
                </Modal>

            </>
        );
    }
}
export default ListarV;
