import 'date-fns';
import { React, Fragment, Component } from 'react';
import app from '../../util/firebase';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import firebase from 'firebase/app';
import 'firebase/database';
import { createMuiTheme } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';

class Home extends Component {
    theme = createMuiTheme({
        palette: {
            primary: {
                light: '#757ce8',
                main: '#3f50b5',
                dark: '#002884',
                contrastText: '#fff',
            },
            secondary: {
                light: '#ff7961',
                main: '#f44336',
                dark: '#ba000d',
                contrastText: '#000',
            },
        },
    });

    state = {
        producto:
        {
            id: '',
            NombrePro: '',
            CostoProv: 0.0,
            CostoVen: 0.0,
            Stock: 10,
            StockMin: 10,
            Tipo: '',
            Detalle: '',
            Fecha: new Date(),
            Marca: '',


        },
        Iva: 0,
        productos: [],
        open: false,
        setOpen: false,
        nodata: false,
        titleButton: 'Guardar',
        aux: ''
    }

    formatDate(date) {
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        return date.toLocaleDateString(undefined, options);
    }
    componentDidMount() {
        this.cargarData();
    }
    cargarData() {
        firebase.database().ref('Producto/').on('value', snapshot => {
            const data = snapshot.val();
            if (data != null) {
                this.setState({ productos: data });
            } else {
                this.setState({ nodata: true });
            }
        });
    }
    onSubmit = (e) => {
        e.preventDefault();
        const { producto } = this.state;
        const id = producto.id === '' ?
            this.state.productos.length : producto.id;
        var newProducto = {
            NombrePro: producto.NombrePro.toUpperCase(),
            CostoProv: producto.CostoProv,
            CostoVen: producto.CostoVen,
            Stock: producto.Stock,
            StockMin: producto.StockMin,
            Tipo: producto.Tipo.toUpperCase(),
            Detalle: producto.Detalle.toUpperCase(),
            Fecha: this.formatDate(this.state.producto.Fecha),
            id,
            Marca: producto.Marca.toUpperCase(),
            Iva: this.state.Iva,

        }
        firebase.database().ref('Producto/' + newProducto.id).set(newProducto);
        this.setState(prevState => ({
            open: true, setOpen: true, nodata: false, titleButton: 'Guardar',
            producto: {
                ...prevState.producto, id: '',
                ...prevState.producto, NombrePro: '', Stock: 0,
                CostoProv: 0, Tipo: '', Detalle: '', CostoVen: 0, Fecha: new Date(), Marca: '', StockMin: 10
            }
        }))
    }


    handleDelete = (id) => {
        firebase.database().ref('Producto/' + id).remove();
        this.setState({ setOpen: true, open: true });
    }

    handleEdit = (id) => {
        firebase.database().ref('Producto/' + id).on('value', snapshot => {
            const data = snapshot.val();
            this.setState({ producto: data.producto, titleButton: 'Editar' });
        });


    }


    onChange = (e) => {
        var { name, value, type } = e.target;
        console.log(value);
        if (type !== 'number') {
            if (name !== 'Tipo') {
                this.setState({
                    producto: {
                        ...this.state.producto,
                        ...this.state.producto, [name]: value
                    }
    
                })
              } else {
                if (value !== 'Hojas' && value !== 'Libros') {
                    console.log('con iva')
                    this.setState({
                        producto: {
                            ...this.state.producto,
                            ...this.state.producto, Tipo: value
                        }
                    })
    
                    this.state.Iva = 0.12;
    
                } else {
                    console.log('sin iva')
                    this.setState({
                        producto: {
                            ...this.state.producto,
                            ...this.state.producto, Tipo: value
                        }
                    })
                    this.state.Iva = 0;
    
                }
    
    
    
            }

        } else {
             value=parseFloat(value);
             
            if (name !== 'Tipo') {
                this.setState({
                    producto: {
                        ...this.state.producto,
                        ...this.state.producto, [name]: value
                    }
    
                })
              } else {
                if (value !== 'Hojas' && value !== 'Libros') {
                    console.log('con iva')
                    this.setState({
                        producto: {
                            ...this.state.producto,
                            ...this.state.producto, Tipo: value
                        }
                    })
    
                    this.state.Iva = 0.12;
    
                } else {
                    console.log('sin iva')
                    this.setState({
                        producto: {
                            ...this.state.producto,
                            ...this.state.producto, Tipo: value
                        }
                    })
                    this.state.Iva = 0;
    
                }
    
    
    
            }
            

        }
        
    }

    handleClose = () => {
        this.setState({
            open: false, setOpen: false
        });
    }
    render() {

        return (
            <>
                <br></br>

                <Container id="contenedorForm" className="contenedorForm" >
                    <Grid container spacing={2} >
                        <Grid className="grid">
                            <Typography color="primary" className="titulos" variant="h6" gutterBottom>Ingresar Producto </Typography>
                            <form onSubmit={this.onSubmit} className="Form">
                                <Grid container spacing={2} >
                                    <TextField onChange={this.onChange} required
                                        id="NombrePro"
                                        name="NombrePro"
                                        label="Nombre Producto"
                                        fullWidth
                                        value={this.state.producto.NombrePro} />
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={this.onChange} required
                                            id="CostoProv"
                                            name="CostoProv"
                                            label="Costo Compra"
                                            type="number"
                                            fullWidth
                                            value={this.state.producto.CostoProv} />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={this.onChange} required
                                            id="CostoVen"
                                            name="CostoVen"
                                            label="Costo Venta"
                                            type="number"
                                            fullWidth
                                            value={this.state.producto.CostoVen} />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={this.onChange} required
                                            id="Stock"
                                            name="Stock"
                                            type="number"
                                            label="Stock"
                                            InputProps={{ inputProps: { min: 0, max: 1000 } }}
                                            fullWidth
                                            value={this.state.producto.Stock} />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={this.onChange} required
                                            id="StockMin"
                                            name="StockMin"
                                            type="number"
                                            InputProps={{ inputProps: { min: 0, max: 1000 } }}
                                            label="Stock_Minimo"
                                            fullWidth
                                            value={this.state.producto.StockMin} />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <InputLabel >Seleccionar</InputLabel>
                                        <InputLabel >Tipo Producto</InputLabel>
                                        <Select onChange={this.onChange} required
                                            id="select"
                                            name="Tipo"
                                            type="text"
                                            label="Tipo de Producto"
                                            fullWidth
                                            value={this.state.producto.Tipo}
                                        >
                                            <MenuItem value={'Libros'}> Libros </MenuItem>
                                            <MenuItem value={'Hojas'}> Hojas </MenuItem>
                                            <MenuItem value={'Cuadernos'}> Cuadernos </MenuItem>
                                            <MenuItem value={'Pliegos'}> Pliegos </MenuItem>
                                            <MenuItem value={'Insumos'}> Insumos de Oficina </MenuItem>
                                            <MenuItem value={'Accesorios'}> Accesorios Informaticos </MenuItem>
                                        </Select>

                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <InputLabel >Marca</InputLabel>
                                        <TextField onChange={this.onChange} required
                                            id="Marca"
                                            name="Marca"
                                            type="Text"
                                            label="Ejm Norma"
                                            fullWidth
                                            value={this.state.producto.Marca} />
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <InputLabel >Detalle</InputLabel>
                                        <TextField onChange={this.onChange} required
                                            id="Detalle"
                                            name="Detalle"
                                            format="Text"
                                            label="Incliur nº hojas"
                                            fullWidth
                                            value={this.state.producto.Detalle} />
                                    </Grid>
                                </Grid>
                                <br></br>
                                <br></br>
                                <Button variant="contained" color="primary" type="submit"> {this.state.titleButton} </Button>
                            </form>
                        </Grid>
                        <Dialog open={this.state.open} onClose={this.handleClose} >
                            <DialogTitle className="dialogo">{"Se ha procesado satisfactoriamente."}</DialogTitle>
                            <DialogActions className="dialogo">
                                <Button onClick={this.handleClose} color={this.theme.palette.secondary.light}>
                                    Cerrar
                        </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                </Container>
            </>
        )
    };
}
export default Home;