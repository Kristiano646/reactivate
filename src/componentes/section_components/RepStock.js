import { React, Component } from 'react';
import { withStyles, createStyles, createMuiTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';

class inventario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aux:[],
            color: "",
            data: [],
            term: 'sin datos',
            producto: [],
            modalEditar: false,
            form: {
                NombrePro: '',
                CostoProv: '',
                CostoVen: '',
                Stock: '',
                Tipo: '',
                Detalle: '',
                Marca: '',
                Fecha: new Date(),
                status: false
            },
            id: 0

        }
    }
    toggle = this.toggle.bind(this);

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
        console.log(this.state.form);
    }
    peticionget = () => {
             firebase.database().ref().child('Producto').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.data, data: datos.val() })
                    this.setState({ ...this.state.producto, producto: datos.val() })
                    this.state.aux=datos.val();
                    
                } else {
                    this.setState({ data: [] })
                }
            })
           


    }
    componentWillMount() {
        this.peticionget();
       
       
    }




   
    seleccionarProducto = async (canal, id, caso) => {
        await this.setState({ form: canal, id: id });
        (caso === "Editar")
            ? this.setState({ modalEditar: true })
            : this.peticionDelete()


    }
    peticionPut = () => {
        firebase.database().ref('Producto/' + this.state.id)
            .set(this.state.form,
                error => {
                    this.setState({ data: this.state.form, titleButton: 'Editar' });
                    console.log(error)
                });
        this.setState({ modalEditar: false });
        window.location.reload()
    }
    filter(event) {
        var text = event.target.value.toUpperCase();
        console.log(this.state.form);
        const data = this.state.data
        const newData = data.filter(function (item) {
            const itemid = item.id
            const itemDataTitle = item.NombrePro.toUpperCase()
            const itemDataDescp = item.Detalle.toUpperCase()
            const itemDataTipo = item.Tipo.toUpperCase()
            const itemDataMarca = item.Marca.toUpperCase()
            const campo = itemid + " " + itemDataTitle + " " + itemDataDescp + " " + itemDataMarca + " " + itemDataTipo;
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            producto: newData,

        })
    }
    imprimir() {
        console.log("llego")
        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        mywindow.document.write('<html><head>');
        mywindow.document.write('<style>.tabla{width:100%;border-collapse:collapse;margin:16px 0 16px 0;}.tabla th{border:1px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;}.tabla td{border:1px solid #ddd;text-align:left;padding:6px;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById("tabla").innerHTML);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necesario para IE >= 10
        mywindow.focus(); // necesario para IE >= 10
        mywindow.print();
        mywindow.close();

    }
    peticionDelete = () => {
        if (
            swal({
                title: "¿Estás seguro que deseas eliminar el Producto",
                text: " " + this.state.form.NombrePro + "?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        firebase.database().ref('Producto/' + this.state.id).remove()
                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino el producto!");
                    }
                }
                )
        );
    }
    comprobarmin= () => {
        if (this.state.aux.Stock <= this.state.aux.StockMin) {
            this.state.color = "red";
        }else{
            this.state.color = "green"
            }
        
    }

    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',

                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });


        const StyledTableCell = withStyles((theme) =>
            createStyles({

                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 14,

                },
                body: {
                    fontSize: 11,



                },

                root: {
                    width: '15%',
                },

            }),
        )(TableCell);

        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,

                    },
                },
            }),
        )(TableRow);
        return (
            <>
                <Button onClick={() => this.imprimir()} variant="contained" color="primary" component="span">
                    Imprimir
              </Button><br></br>
                <SearchTwoToneIcon className="iconoBusqueda" color="primary" />
                <input class="busqueda"
                    value={this.state.text}
                    onChange={(text) => this.filter(text)} />
                <TableContainer className="contenedorTabla" id="tabla">
                    <Table className="tabla" >
                        <TableHead >
                            <TableRow>
                                <StyledTableCell >Codigo</StyledTableCell>
                                <StyledTableCell >Producto</StyledTableCell>
                                <StyledTableCell >Stock</StyledTableCell>
                                <StyledTableCell >Stock Minimo</StyledTableCell>
                                <StyledTableCell >Tipo</StyledTableCell>
                                <StyledTableCell >Detalle</StyledTableCell>
                                <StyledTableCell >Marca</StyledTableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody className="TableBody">
                            {!this.props.NoData
                                ? Object.keys(this.state.producto).map(i => {
                                    return (
                                        <StyledTableRow hover key={i}>
                                            <StyledTableCell align="left">{this.state.producto[i].id}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].NombrePro}</StyledTableCell>
                                           
                                            
                                            { !(this.state.producto[i].Stock<=this.state.producto[i].StockMin)
                                              ?<StyledTableCell
                                              style={{color:'green'}}
                                              lign="left">{this.state.producto[i].Stock}
                                              </StyledTableCell>
                                              :<StyledTableCell
                                              style={{color:'red'}}
                                              lign="left">{this.state.producto[i].Stock}
                                              </StyledTableCell>                                             
                                            }
                                             <StyledTableCell align="left"><p>{this.state.producto[i].StockMin}                 ...........<BorderColorRoundedIcon fontSize="small" onClick={() => this.seleccionarProducto(this.state.data[i], i, 'Editar')} color="primary" />
                                            </p>   </StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Tipo}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Detalle}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Marca}</StyledTableCell>
                                        </StyledTableRow>
                                    )
                                })
                                : 'sin datos'}
                        </TableBody>
                    </Table>

                </TableContainer>
                <Modal toggle={this.toggle} isOpen={this.state.modalEditar} className="Modal">
                    <Container  >
                        <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom>Editar Producto </Typography></ModalHeader>
                        <ModalBody>
                            <Grid container spacing={2} >
                                <Grid className="grid">
                                    <form onSubmit={this.onSubmit}>
                                        <Grid container spacing={2} >
                                            <TextField onChange={this.handleChange} required
                                                id="NombrePro"
                                                name="NombrePro"
                                                label="Nombre Producto"
                                                fullWidth
                                                value={this.state.form.NombrePro} />
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="CostoProv"
                                                    name="CostoProv"
                                                    label="Costo Compra"
                                                    type="number"
                                                    fullWidth
                                                    value={this.state.form.CostoProv} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="CostoVen"
                                                    name="CostoVen"
                                                    label="Costo Venta"
                                                    type="number"
                                                    fullWidth
                                                    value={this.state.form.CostoVen} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="Stock"
                                                    name="Stock"
                                                    type="number"
                                                    label="Stock"
                                                    fullWidth
                                                    value={this.state.form.Stock} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField onChange={this.handleChange} required
                                                    id="StockMin"
                                                    name="StockMin"
                                                    type="number"
                                                    label="StockMin"
                                                    fullWidth
                                                    value={this.state.form.StockMin} />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <label>Tipo*</label>
                                                <Select name="Tipo"
                                                    id="Tipo"
                                                    name="Tipo"
                                                    format="Text"
                                                    type="Text"
                                                    fullWidth
                                                    value={this.state.form.Tipo}
                                                    onChange={this.handleChange}>
                                                    <MenuItem value={'LIBROS'}> LIBROS</MenuItem>
                                                    <MenuItem value={'HOJAS'}> HOJAS </MenuItem>
                                                    <MenuItem value={'CUADERNOS'}> CUADERNOS </MenuItem>
                                                    <MenuItem value={'PLIEGOS'}> PLIEGOS </MenuItem>
                                                    <MenuItem value={'ACCESORIOS'}> ACCESORIOS </MenuItem>
                                                </Select>
                                            </Grid>
                                            <Grid item xs={12} sm={8} >
                                                <TextField onChange={this.handleChange} required
                                                    id="Detalle"
                                                    name="Detalle"
                                                    format="Text"
                                                    type="Text"
                                                    label="Detalle"
                                                    fullWidth
                                                    value={this.state.form.Detalle} />
                                            </Grid>
                                        </Grid>
                                    </form>
                                </Grid>
                            </Grid>
                        </ModalBody>
                        <ModalFooter className="ModalFooter">
                            <br></br><br></br>
                            <Button variant="contained" color="primary" onClick={() => this.peticionPut()}>Enviar</Button>{"   "}
                            <Button variant="contained" color="primary" onClick={() => this.setState({ modalEditar: false })}>Cancelar</Button>

                        </ModalFooter>
                    </Container>
                </Modal>

            </>
        );
    }
}
export default inventario;




