import { React, Component } from 'react';
import { withStyles, createStyles, createMuiTheme, makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';
import Icon from '@material-ui/core/Icon';
import li from './imagenes/+.png';
import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import { date } from 'date-fns/locale/af';
import { blue } from '@material-ui/core/colors';






class repventas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cliente: [],
            term: 'sin datos',
            modalver: false,
            form: {
                NombrePro: '',
                CostoProv: '',
                CostoVen: '',
                Stock: '',
                Tipo: '',
                Detalle: '',
                Marca: '',
                Fecha: new Date(),
                status: false
            },
            id: 0,
            ids: 0,
            producto: [],
            data: [],
            FechaIni:'',
            FechaF:'',
            nodata: false,
        }
    }


    toggle = this.toggle.bind(this);

    toggle() {
        this.setState({
            modalver: !this.state.modal
        });
    }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
        console.log(this.state.form);
    }
    cargarData() {
        firebase.database().ref('Venta/').on('value', snapshot => {
            const data = snapshot.val();
            if (data != null) {
                this.setState({ ...this.state.data, data: data });
            } else {
                this.setState({ nodata: true });
            }
        });
    }
    traerventapro = (i) => {

        firebase.database().ref('Venta').child(i).child('producto').
            on('value', datos => {

                this.setState({ ...this.state.data, data: datos.val() });
            })
        this.setState({ modalver: true, ids: i })
    }
    traerventa = () => {
        firebase.database().ref('Venta').
            on('value', datos => {

                if (datos.val() !== null) {

                    this.setState({ ...this.state.producto, producto: datos.val() })

                } else {
                    this.setState({ data: [] })
                }
            })
        firebase.database().ref().child('Cliente').on('value', datos => {

            if (datos.val() !== null) {
                this.setState({ cliente: datos.val() })
            } else {
                this.setState({ cliente: [] })
            }
        })
        console.log(this.state.cliente)


    }


    componentWillMount() {
        this.traerventa();
       

    }

    filter(event) {
        var text = event.target.value.toUpperCase();
        const data = Object.values(this.state.producto)
        console.log(this.state.producto)
        const newData = data.filter(function (item) {
            const itemcedula = item.cedula
            const itemnombre = item.Nombre.toUpperCase()
            const campo = itemcedula + " " + itemnombre;
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            producto: newData,

        })
    }
    formatDate(date) {
        
        const options = {  year: 'numeric', month: 'numeric', day: 'numeric' };
        return date.toLocaleDateString(undefined, options);
    }
    fechasF = e => {
        let texto=e.target.value;
        let cad=texto.replace('-','/');
         cad=cad.replace('-','/');
        console.log(cad)
        console.log(this.state.producto[17].Fecha)
     /*   let texto=e.target.value;
        let cad=texto.replace('-',',');
         cad=cad.replace('-',',');
       
          this.state.fechasF=this.formatDate(new Date(cad));
          console.log( this.state.fechasF)*/
          this.filtrofechas();
      }
      fechasIni = e => {
        this.traerventa();
        let texto=e.target.value;
        let cad=texto.replace('-','/');
         cad=cad.replace('-','/');
        // console.log(this.formatDate(new Date(cad)));
      this.state.fechasIni=cad;
      this.filtrofechas();
      }

    filtrofechas(){
        
        console.log(this.state.fechasIni);
        console.log(this.state.fechasF);
         var text1 = this.state.fechasIni;
        var text2 = this.state.fechasF;
        const data = Object.values(this.state.producto)
        const newData = data.filter(function (item) {
            const itemFecha = item.Fecha    
            const campo = itemFecha;
            const textData1 = text1;
            const textData2 = text2;
             return campo.indexOf(textData1)>-1
        })
        this.setState({
            producto: newData,

        })
    
   
    }
    



    imprimir() {
        console.log("llego")
        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        mywindow.document.write('<html><head>');
        mywindow.document.write('<style>.tabla{width:100%;border-collapse:collapse;margin:16px 0 16px 0;}.tabla th{border:1px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;}.tabla td{border:1px solid #ddd;text-align:left;padding:6px;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById("tabla").innerHTML);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necesario para IE >= 10
        mywindow.focus(); // necesario para IE >= 10
        mywindow.print();
        mywindow.close();

    }

    render() {
        var sutotal = 0;
        function ccyFormat(num) {
            return `${num.toFixed(2)}`;
          }
        var sumat=0
        function suma(val) {

            sumat = sumat + val;
            return sumat;
          }
          

        const f=new Date();
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',

                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });

        const StyledTableCell = withStyles((theme) =>
            createStyles({

                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 14,

                },
                body: {
                    fontSize: 11,



                },

                root: {
                    width: '15%',
                },

            }),
        )(TableCell);

        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,

                    },
                },
            }),
        )(TableRow);
        return (
            <>
                <form className="Fcontenedor" noValidate>
                    <TextField onChange={this.fechasIni}
                        id="date"
                        label="Elija"
                        type="date"
                        className="calendario"
                        InputLabelProps={{
                            shrink: true,
                        }} />
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <TextField onChange={this.fechasF}
                        id="date"
                        label="Hasta"
                        type="date"
                        className="calendario"
                        style={{display:'none'}}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        
                         />
    
                </form>
               


                <ThreeSixtyIcon onClick={() => this.traerventa()} className="iconoreload" color="primary" />
                <input class="busqueda"
                    value={this.state.text}
                    onChange={(text) => this.filter(text)} />

                <TableContainer className="contenedorTabla" id="tabla">
                    <Table className="tabla" >
                        <TableHead >
                            <TableRow
                            >
                                <StyledTableCell >Cedula</StyledTableCell>
                                <StyledTableCell >Nombre</StyledTableCell>
                                <StyledTableCell >Direccon</StyledTableCell>
                                <StyledTableCell >Email</StyledTableCell>
                                <StyledTableCell >Fecha</StyledTableCell>
                                <StyledTableCell >Productos</StyledTableCell>
                                <StyledTableCell >Monto</StyledTableCell>


                            </TableRow>
                        </TableHead>
                        <TableBody className="TableBody">
                            {!this.props.NoData
                                ? Object.keys(this.state.producto).map(i => {
                                    return (
                                        <>
                                        <StyledTableRow hover key={i}>
                                            <StyledTableCell align="left">{this.state.producto[i].cedula}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Nombre}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].direccion}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].email}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Fecha}</StyledTableCell>
                                            <StyledTableCell align="left">
                                                <Icon color="primary"><img onClick={() => this.traerventapro(i)} id="icono" src={li} className="mas"></img></Icon>
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Total}</StyledTableCell>
                                            <StyledTableCell align="left" style={{display:'none'}} >{
                                            ccyFormat(
                                                sutotal=suma(Number(this.state.producto[i].Total)))}</StyledTableCell>
                                        
                                        </StyledTableRow>
                                        
                                        </>
                                         
                                    )

                                })
                                : 'sin datos'}
                        </TableBody>
                    </Table>
                    <center>
                    <div style={{backgroundColor:'blue', color:'white'}}>
                    <th >Venta Total </th><br></br>
                    <td>{sutotal}</td>
                    </div>
                    </center>
                </TableContainer>

                <Modal toggle={this.toggle} isOpen={this.state.modalver} className="Modal">
                    <Container  >
                        <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom>Productos</Typography></ModalHeader>
                        <ModalBody>
                            <br></br>
                            <TableContainer className="contenedorTabla" id="tabla">
                                <Table className="tabla" >
                                    <StyledTableRow hover key={this.state.data.id}>
                                        <TableHead >
                                            <StyledTableRow>
                                                <StyledTableCell align="center">Nombre</StyledTableCell>
                                                <StyledTableCell align="center">Detalle</StyledTableCell>
                                                <StyledTableCell align="center">Cantidad</StyledTableCell>
                                                <StyledTableCell align="center">Precio</StyledTableCell>
                                            </StyledTableRow>
                                        </TableHead>
                                        <TableBody>
                                            {Object.keys(this.state.data).map(i => {
                                                return (
                                                    <StyledTableRow>

                                                        <StyledTableCell align="center">{this.state.data[i].NombrePro}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Detalle}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Cantidad}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].CostoVen}</StyledTableCell>
                                                    </StyledTableRow>
                                                )
                                            })
                                            }
                                        </TableBody>
                                    </StyledTableRow>
                                </Table>
                            </TableContainer>
                        </ModalBody>
                        <ModalFooter className="ModalFooter">
                            <br></br><br></br>
                            <Button variant="contained" color="primary" onClick={() => this.setState({ modalver: false })}>Cancelar</Button>

                        </ModalFooter>
                    </Container>
                </Modal>



            </>
        );
    }
}
export default repventas;



/*  {!this.props.NoData
                                   ? Object.keys(this.state.data).map(i => {
                                       return(
                                       <StyledTableRow>

                                            <StyledTableCell align="left">{this.state.data.producto[i].NombrePro}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].Detalle}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].Cantidad}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].CostoVen}</StyledTableCell>
                                        </StyledTableRow>
                                    )
                                    })
                                : 'sin datos'}*/

                                /* FIlTRO UNA FECHA
                                
                                 console.log(this.state.fechasIni);
          console.log(this.state.fechasF);
           var text1 = this.state.fechasIni;
          var text2 = this.state.fechasF;
          const data = Object.values(this.state.producto)
          const newData = data.filter(function (item) {
              const itemFecha = item.Fecha    
              const campo = itemFecha;
              const textData1 = text1;
              const textData2 = text2;
               return campo.indexOf(textData1)>-1
          })
          this.setState({
              producto: newData,
  
          })

*/