import { React, Fragment, Component } from 'react';
import app from '../../util/firebase';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import firebase from 'firebase/app';
import 'firebase/database';
import { createMuiTheme } from '@material-ui/core/styles';


class Proveedor extends Component {
    theme = createMuiTheme({
        palette: {
            primary: {
                light: '#757ce8',
                main: '#3f50b5',
                dark: '#002884',
                contrastText: '#fff',
            },
            secondary: {
                light: '#ff7961',
                main: '#f44336',
                dark: '#ba000d',
                contrastText: '#000',
            },
        },
    });
// Json este es el objeto
    state = {
        proveedor:
        {
            id: '',
            NombreProv: '',
            TelefonoProv: '',
            CorreoProv: '',
            status: false
        },
        proveedor1: [],
        open: false,
        setOpen: false,
        nodata: false,
        titleButton: 'Guardar'
    }

    componentDidMount() {
        this.cargarDataProv();
    }
   
    cargarDataProv() {
        firebase.database().ref('Proveedor/').on('value', snapshot => {
            const data = snapshot.val();
            if (data != null) {
                this.setState({ proveedor1: data });
            } else {
                this.setState({ nodata: true });
            }
        });
    }
    onSubmit = (e) => {
        e.preventDefault();
        const { proveedor } = this.state;
        const id = proveedor.id === '' ?
            this.state.proveedor1.length : proveedor.id;
        var newProveedor = {
            
                NombreProv: proveedor.NombreProv.toUpperCase(),
                TelefonoProv: proveedor.TelefonoProv,
                CorreoProv: proveedor.CorreoProv.toUpperCase(),
                id,
                status: proveedor.status
           
        }
        
       //se saca del objeto proveedor1
        firebase.database().ref('Proveedor/' + newProveedor.id).set(newProveedor);
        
        this.setState(prevState => ({ open: true, setOpen: true, nodata: false, titleButton: 'Guardar',
        proveedor:{        
        ...prevState.proveedor, id: '',
                ...prevState.proveedor, NombreProv: '',TelefonoProv: '', CorreoProv: ''
        }
        }))
    }


    handleDelete = (id) => {
        firebase.database().ref('Proveedor/' + id).remove();
        this.setState({ setOpen: true, open: true });
    }

    handleEdit = (id) => {
        firebase.database().ref('Proveedor/' + id).on('value', snapshot => {
            const data = snapshot.val();
            this.setState({ proveedor: data.proveedor, titleButton: 'Editar' });
        });

 
    }
    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            proveedor: {
                ...this.state.proveedor,
                ...this.state.proveedor, [name]: value
            }
        });
    }
    
        handleClose = () => {
            this.setState({
                open: false, setOpen: false
            });
        }
        render() {
            return (

                <Container id="contenedorForm" className="contenedorForm" >
                    <Grid container spacing={2} >
                        <Grid className="grid">
                            <Typography color="primary" className="titulos" variant="h6" gutterBottom>Ingresar Proveedor </Typography>
                            <form onSubmit={this.onSubmit}>
                                <Grid container spacing={2} >
                                    <Grid item xs={12} sm={6}>
                                    <TextField onChange={this.onChange} required
                                        id="NombreProv"
                                        name="NombreProv"
                                        label="Nombre Proveedor"
                                        fullWidth
                                        value={this.state.proveedor.NombreProv} />
                                    </Grid>   
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={this.onChange} required
                                            id="TelefonoProv"
                                            name="TelefonoProv"
                                            label="Teléfono Proveedor"
                                            fullWidth
                                            value={this.state.proveedor.TelefonoProv} />
                                    </Grid>
                                    <Grid item xs={12} sm={8} >
                                            <TextField onChange={this.onChange} required
                                            id="CorreoProv"
                                            name="CorreoProv"
                                            label="Correo Proveedor"
                                            fullWidth
                                            value={this.state.proveedor.CorreoProv} />
                                    </Grid>
                                    <Grid item xs={12} sm={8}>
                                        <Button variant="contained" color="primary" type="submit"> {this.state.titleButton} </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>
                        <Dialog open={this.state.open} onClose={this.handleClose} >
                            <DialogTitle className="dialogo">{"Se almacenó adecuadamente la información."}</DialogTitle>
                            <DialogActions className="dialogo">
                                <Button onClick={this.handleClose} color={this.theme.palette.secondary.light}>
                                    Cerrar
                        </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                </Container>
            )
        };
    }
    export default Proveedor;