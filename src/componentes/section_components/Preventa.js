import React, { Fragment, useState, useCallback, Component } from 'react';
import ReactDOM from 'react-dom';
import { makeStyles, withStyles, createStyles, createMuiTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import { icon } from '@fortawesome/fontawesome-svg-core';
import li from './imagenes/+.png';

import Case from '../case';
import { ContactSupportOutlined } from '@material-ui/icons';
class preventa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ven: [],
            val: '',
            data: [],
            datas:[],
            modalVender: false,
            term: 'sin datos',
            producto: [],
           
            id: '',
            cantidadp: {
                cantidad: 0,

            },
            newPedido:[],

            venta: [],
            cli:[],
            Estado:'',


        };
    }
    toggle = this.toggle.bind(this);
    cargarData = this.cargarData.bind(this);
    toggle() {
        this.setState({
            modal: !this.state.modal
        });

    }
    agregarventa = async (canal) => {
        console.log(canal)
        await this.setState({ cantidad: canal });
        /*this.setState({ modalVender: true })*/
        var newPedido = {
            NombrePro: this.state.ven.NombrePro.toUpperCase(),
            CostoVen: this.state.ven.CostoVen,
            Stock: this.state.ven.Stock,
            Detalle: this.state.ven.Detalle.toUpperCase(),
            Fecha: this.state.ven.Fecha,
            id: this.state.ven.id,
            tipo: this.state.ven.Tipo,
            Cantidad: this.state.cantidadp.cantidad,
          

        }
        firebase.database().ref('Venta/' + newPedido.id).set(newPedido);

    }
    peticionget = () => {
        firebase.database().ref().child('VentaRes').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.data, data: datos.val() })
                    this.state.data=datos.val();
                    this.setState({ ...this.state.producto, producto: datos.val() })
                    
                } else {
                    this.setState({ data: [] })
                }
            })

           console.log(this.state.data);
               
    }

    filter(event) {
        var text = event.target.value.toUpperCase();
        console.log(this.state.data);
        const data = this.state.data
        const newData = data.filter(function (item) {
            const itemid = item.id
            const itemDataTitle = item.NombrePro.toUpperCase()
            const itemDataDescp = item.Detalle.toUpperCase()
            const itemDataTipo = item.Tipo.toUpperCase()
            const itemDataMarca = item.Marca.toUpperCase()
            const campo = itemid + " " + itemDataTitle + " " + itemDataDescp + " " + itemDataTipo + " " + itemDataMarca + " ";
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            producto: newData,

        })
    }



    peticionDelete = () => {
        if (
            swal({
                title: "¿Estás seguro que deseas eliminar el Producto",
                text: " " + this.state.form.NombrePro + "?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        firebase.database().ref('Pedido/' + this.state.id).remove()
                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino el producto!");
                    }
                }
                )
        );
    }



    peticiongetPedido = () => {

        firebase.database().ref().child('Pedido').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.ven, ven: datos.val() })

                } else {
                    this.setState({ data: [] })
                }
            })

    }
    componentWillMount() {
        
        this.peticionget();
        this.cargarData();
        Object.keys(this.state.data).map(i => {
            this.state.Estado=this.state.data[i].Estado
           
            })
            

    }


    mostrarPedido() {
        this.peticiongetPedido();
        this.setState({ modalVender: true });
        this.cargarData();
    }
    seleccionarProducto2 = async (canal, id, caso) => {
        await this.setState({ form: canal, id: id });
        (caso === "Editar")
            ? this.setState({ modalEditar: true })
            : this.peticionDelete()
    }
    seleccionarProducto () {
        
        this.peticiongetPedido();
       
       // this. traerventapro();
             var newPedido = {
              /*  NombrePro: this.state.form.NombrePro.toUpperCase(),
                CostoVen: this.state.form.CostoVen,
                Stock: this.state.form.Stock - this.state.cantidadp.cantidad,
                Detalle: this.state.form.Detalle.toUpperCase(),
                Fecha: this.state.form.Fecha,*/
                //id: 2,
              /*  Tipo: this.state.form.Tipo,
                Cantidad: this.state.cantidadp.cantidad,
                Iva:this.state.form.Iva*/
            }
           // firebase.database().ref('Pedido/' + this.state.newPedido.id).set(this.state.newPedido);
            firebase.database().ref('Pedido/').set(this.state.datas);
            
            firebase.database().ref('ClientePed/').set(this.state.cli);
           // this.state.form.Stock = this.state.form.Stock - this.state.cantidadp.cantidad;
           // firebase.database().ref('Producto/' + this.state.producto.id).set(this.state.form);

      
        
    }
   
      
    traerventapro = (i) => {
this. traerCliente(i);
        firebase.database().ref('VentaRes').child(i).child('producto').
            on('value', datos => {
this.state.datas=datos.val();
this.state.data=datos.val();

                //this.setState({ ...this.state.data, data: datos.val() });
            })
            
        this.setState({ modalver: true, ids: i })
    }

    traerCliente = (i) => {
        
                firebase.database().ref('VentaRes').child(i).
                    on('value', datos => {
        this.state.cli=datos.val();

        
                        //this.setState({ ...this.state.data, data: datos.val() });
                    })
                    
                this.setState({ modalver: true, ids: i })
            }


    cancelar = async (canal) => {
        if (
            await swal({
                title: "¿Estás seguro que deseas eliminar el Producto",
                text: " " + this.state.form.NombrePro + "del pedido?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        let cant = 0;
                        cant = this.state.cantidadp.cantidad;
                        let sto = 0;
                        sto = this.state.form.Stock;
                        let stk = (Number(sto) + Number(cant));
                        this.state.form.Stock = stk;
                        firebase.database().ref('Producto/' + this.state.form.id).set(this.state.form);
                        firebase.database().ref('Pedido/' + this.state.id).remove();
                        this.setState({ modalVender: false })

                        swal("Eliminado!", {
                            icon: "success",
                        })
                    } else {

                        swal("No se elimino el producto!");
                    }
                }
                )
        );
    }

    cantidad = e => {
        console.log(this.state.producto[e.target.id].Stock);
       if(this.state.producto[e.target.id].Stock < e.target.value){
       
        swal({
            title: "Stock Insuficiente",
            text: " " + this.state.producto[e.target.id].NombrePro + " sin Stock....!!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        e.target.value='';
        
       }else{
       
        this.state.cantidadp.cantidad = e.target.value
       }
    }
    cargarData() {
        firebase.database().ref('Pedido/').on('value', datos => {
            if (datos.val() !== null) {
                this.setState({ ...this.state.venta, venta: datos.val() })

            } else {
                this.setState({ venta: [] })
            }
        });

    }

    enviar = (i) => {
        
        this.traerventapro(i);
        this.seleccionarProducto();
        document.getElementById('Ventas').style.display = 'none';
        document.getElementById('Factura').style.display = 'inline';
        this.setState({ modalver: false })


    }

  habilitar(i){
    
      if (this.state.producto[i].Estado==='Pendiente'){
          return false
      }else
      return true
  }

    render() {
        var sutotal = 0;
        function ccyFormat(num) {
            return `${num.toFixed(2)}`;
          }
          var sumat=0
          function suma(val) {
  
              sumat = sumat + val;
              return sumat;
            }
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',
                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });
        const StyledTableCell = withStyles((theme) =>
            createStyles({
                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 14,
                },
                body: {
                    fontSize: 11,
                },
                root: {
                    width: '15%',
                },
            }),
        )(TableCell);
        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,
                    },
                },
            }),
        )(TableRow);

        return (
            <>
                <div id="Ventas">
                    <SearchTwoToneIcon className="iconoBusqueda" color="primary" />
                    <input class="busqueda"
                        value={this.state.text}
                        onChange={(text) => this.filter(text)} />
                    <br></br>
                   
                    <br></br>
                    <br></br>
                    <TableContainer className="contenedorTabla">
                        <Table className="tabla">
                            <TableHead >
                                <TableRow >
                                    <StyledTableCell >Cédula</StyledTableCell>
                                    <StyledTableCell >Nombre</StyledTableCell>
                                    <StyledTableCell >Direccion</StyledTableCell>
                                    <StyledTableCell >Email</StyledTableCell>
                                    <StyledTableCell >Fecha</StyledTableCell>
                                    <StyledTableCell >Estado</StyledTableCell>
                                    <StyledTableCell >Productos</StyledTableCell>
                                    <StyledTableCell >Valor</StyledTableCell>
                                    <StyledTableCell >Accion</StyledTableCell>
                                  
                                </TableRow>
                            </TableHead>
                            <TableBody className="TableBody">
                            {!this.props.NoData
                                ? Object.keys(this.state.producto).map(i => {
                                    return (
                                        <>
                                        <StyledTableRow hover key={i}>
                                            <StyledTableCell align="left">{this.state.producto[i].cedula}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Nombre}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].direccion}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].email}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Fecha}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Estado}</StyledTableCell>
                                            <StyledTableCell align="left">
                                                <Icon color="primary"><img onClick={() => this.traerventapro(i)} id="icono" src={li} className="mas"></img></Icon>
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Total}</StyledTableCell>
                                            <Button style={{marginTop:'20px'}} disabled={this.habilitar(i)} variant="contained" color="primary" onClick={() => this.enviar(i)}>Facturar</Button>
                                            <StyledTableCell align="left" style={{display:'none'}} >{
                                            ccyFormat(
                                                sutotal=suma(Number(this.state.producto[i].Total)))}</StyledTableCell>
                                         
                                

                                        </StyledTableRow>
                                        
                                        </>
                                         
                                    )

                                })
                                : 'sin datos'}
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <Modal toggle={this.toggle} isOpen={this.state.modalver} className="Modal">
                    <Container  >
                        <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom>Productos</Typography></ModalHeader>
                        <ModalBody>
                            <br></br>
                            <TableContainer className="contenedorTabla" id="tabla">
                                <Table className="tabla" >
                                    <StyledTableRow hover key={this.state.data.id}>
                                        <TableHead >
                                            <StyledTableRow>
                                                <StyledTableCell align="center">Nombre</StyledTableCell>
                                                <StyledTableCell align="center">Detalle</StyledTableCell>
                                                <StyledTableCell align="center">Cantidad</StyledTableCell>
                                                <StyledTableCell align="center">Precio</StyledTableCell>
                                            </StyledTableRow>
                                        </TableHead>
                                        <TableBody>
                                            {Object.keys(this.state.data).map(i => {
                                                return (
                                                    <StyledTableRow>

                                                        <StyledTableCell align="center">{this.state.data[i].NombrePro}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Detalle}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Cantidad}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].CostoVen}</StyledTableCell>
                                                    </StyledTableRow>
                                                )
                                            })
                                            }
                                        </TableBody>
                                    </StyledTableRow>
                                </Table>
                            </TableContainer>
                        </ModalBody>
                        <ModalFooter className="ModalFooter">
                            <br></br><br></br>
                            <Button variant="contained" color="primary" onClick={() => this.setState({ modalver: false })}>Cerrar</Button>

                        </ModalFooter>
                    </Container>
                </Modal>

                <Modal toggle={this.toggle} isOpen={this.state.modalVender} className="Modal2" cargarData={this.cargarData}>
                        <Container  >
                            <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom> Formulario de Venta </Typography></ModalHeader>
                            <br></br>
                            <ModalBody>
                                <Grid container spacing={2} >
                                    <Grid className="grid">
                                    </Grid>
                                </Grid>
                            </ModalBody>
                            <ModalFooter className="ModalFooter">
                                <TableContainer className="contenedorTabla">
                                    <Table className="tabla">
                                        <TableHead >
                                            <TableRow >
                                                <StyledTableCell >Producto</StyledTableCell>
                                                <StyledTableCell >PVP</StyledTableCell>
                                                <StyledTableCell >Cantidad</StyledTableCell>
                                                <StyledTableCell >Tipo</StyledTableCell>
                                                <StyledTableCell >Detalle</StyledTableCell>
                                                <StyledTableCell >Accion</StyledTableCell>

                                            </TableRow>
                                        </TableHead>
                                        <TableBody className="TableBody">
                                            {!this.props.NoData
                                                ? Object.keys(this.state.ven).map(i => {
                                                    return (
                                                        <StyledTableRow hover key={i}>
                                                            <StyledTableCell align="left">{this.state.ven[i].NombrePro}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].CostoVen}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Cantidad}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Tipo}</StyledTableCell>
                                                            <StyledTableCell align="left">{this.state.ven[i].Detalle}</StyledTableCell>
                                                            <StyledTableCell align="left">
                                                                <DeleteRoundedIcon fontSize="small" color="primary" onClick={() => this.seleccionarProducto(this.state.producto[i], i, 'Eliminar')} />
                                                            </StyledTableCell>
                                                        </StyledTableRow>
                                                    )
                                                })
                                                : 'sin datos'}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <br></br>
                                <Button variant="contained" color="primary" onClick={() => this.enviar()}>Facturarñ</Button>{"   "}
                                <Button variant="contained" color="primary" onClick={() => this.setState({ modalVender: false })}>Adicionar</Button>
                            </ModalFooter>
                        </Container>
                    </Modal>

                </div>
                <div id="Factura" className="Factura">
                    <Case status="FacturaPre"/>
                </div>






            </>
        )
    }
}
export default preventa;




















































/*import { React, Component } from 'react';
import { withStyles, createStyles, createMuiTheme, makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import firebase from 'firebase/app';
import 'firebase/database';
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import swal from 'sweetalert';
import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone';
import Icon from '@material-ui/core/Icon';
import li from './imagenes/+.png';
import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import { date } from 'date-fns/locale/af';
import { blue } from '@material-ui/core/colors';

import Case from '../case';
class Preventas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cliente: [],
            term: 'sin datos',
            modalver: false,
            form: {
                NombrePro: '',
                CostoProv: '',
                CostoVen: '',
                Stock: '',
                Tipo: '',
                Detalle: '',
                Marca: '',
                Fecha: new Date(),
                status: false
            },
            id: 0,
            ids: 0,
            producto: [],
            data: [],
            FechaIni:'',
            FechaF:'',
            nodata: false,
        }
    }


    toggle = this.toggle.bind(this);

    toggle() {
        this.setState({
            modalver: !this.state.modal
        });
    }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
        console.log(this.state.form);
    }
    cargarData() {
        firebase.database().ref('VentaRes/').on('value', snapshot => {
            const data = snapshot.val();
            if (data != null) {
                this.setState({ ...this.state.data, data: data });
            } else {
                this.setState({ nodata: true });
            }
        });
    }
    peticiongetPedido = () => {

        firebase.database().ref().child('VentaRes/').
            on('value', datos => {

                if (datos.val() !== null) {
                    this.setState({ ...this.state.ven, ven: datos.val() })

                } else {
                    this.setState({ data: [] })
                }
            })

    }
    traerventapro = (i) => {

        firebase.database().ref('VentaRes').child(i).child('producto').
            on('value', datos => {
               
                this.setState({ ...this.state.data, data: datos.val() });
            })
        this.setState({ modalver: true, ids: i })
    }
    traerventa = () => {
        firebase.database().ref('VentaRes').
            on('value', datos => {
                
                if (datos.val() !== null) {

                    this.setState({ ...this.state.producto, producto: datos.val() })

                } else {
                    this.setState({ data: [] })
                }
            })
        firebase.database().ref().child('ClientePre').on('value', datos => {

            if (datos.val() !== null) {
                firebase.database().ref('Cliente/').set(datos.val());
                this.setState({ cliente: datos.val() })

            } else {
                this.setState({ cliente: [] })
            }
        })
       


    }


    componentWillMount() {
        this.traerventa();
       

    }

    filter(event) {
        var text = event.target.value.toUpperCase();
        const data = Object.values(this.state.producto)
        console.log(this.state.producto)
        const newData = data.filter(function (item) {
            const itemcedula = item.cedula
            const itemnombre = item.Nombre.toUpperCase()
            const campo = itemcedula + " " + itemnombre;
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            producto: newData,

        })
    }
    formatDate(date) {
        
        const options = {  year: 'numeric', month: 'numeric', day: 'numeric' };
        return date.toLocaleDateString(undefined, options);
    }
    fechasF = e => {
        let texto=e.target.value;
        let cad=texto.replace('-','/');
         cad=cad.replace('-','/');
        console.log(cad)
        console.log(this.state.producto[17].Fecha)
     //   let texto=e.target.value;
      //  let cad=texto.replace('-',',');
       //  cad=cad.replace('-',',');
      // 
        //  this.state.fechasF=this.formatDate(new Date(cad));
        //  console.log( this.state.fechasF)
          this.filtrofechas();
      }
      fechasIni = e => {
        this.traerventa();
        let texto=e.target.value;
        let cad=texto.replace('-','/');
         cad=cad.replace('-','/');
        // console.log(this.formatDate(new Date(cad)));
      this.state.fechasIni=cad;
      this.filtrofechas();
      }

    filtrofechas(){
        
        console.log(this.state.fechasIni);
        console.log(this.state.fechasF);
         var text1 = this.state.fechasIni;
        var text2 = this.state.fechasF;
        const data = Object.values(this.state.producto)
        const newData = data.filter(function (item) {
            const itemFecha = item.Fecha    
            const campo = itemFecha;
            const textData1 = text1;
            const textData2 = text2;
             return campo.indexOf(textData1)>-1
        })
        this.setState({
            producto: newData,

        })
    
   
    }
    
    enviar = () => {



        document.getElementById('Ventas').style.display = 'none';
        document.getElementById('Factura').style.display = 'inline';
        this.setState({ modalVender: false });


    }


    imprimir() {
        console.log("llego")
        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        mywindow.document.write('<html><head>');
        mywindow.document.write('<style>.tabla{width:100%;border-collapse:collapse;margin:16px 0 16px 0;}.tabla th{border:1px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;}.tabla td{border:1px solid #ddd;text-align:left;padding:6px;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById("tabla").innerHTML);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necesario para IE >= 10
        mywindow.focus(); // necesario para IE >= 10
        mywindow.print();
        mywindow.close();

    }

    render() {
        var sutotal = 0;
        function ccyFormat(num) {
            return `${num.toFixed(2)}`;
          }
        var sumat=0
        function suma(val) {

            sumat = sumat + val;
            return sumat;
          }
          

        const f=new Date();
        const theme = createMuiTheme({
            palette: {
                primary: {
                    light: '#6103F0',
                    main: '#651fff',
                    dark: '#6103F0',

                },
                secondary: {
                    light: '#ff7961',
                    main: '#651fff',
                    dark: '#ba000d',
                    contrastText: '#000',
                },
            },
        });

        const StyledTableCell = withStyles((theme) =>
            createStyles({

                head: {
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.common.white,
                    fontSize: 14,

                },
                body: {
                    fontSize: 11,



                },

                root: {
                    width: '15%',
                },

            }),
        )(TableCell);

        const StyledTableRow = withStyles((theme) =>
            createStyles({
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: theme.palette.action.hover,

                    },
                },
            }),
        )(TableRow);
        return (
            <>
                
               

                <div id="Ventas">

                <ThreeSixtyIcon onClick={() => this.traerventa()} className="iconoreload" color="primary" />
                <input class="busqueda"
                    value={this.state.text}
                    onChange={(text) => this.filter(text)} />

                <TableContainer className="contenedorTabla" id="tabla">
                    <Table className="tabla" >
                        <TableHead >
                            <TableRow
                            >
                                <StyledTableCell >Cedula</StyledTableCell>
                                <StyledTableCell >Nombre</StyledTableCell>
                                <StyledTableCell >Direccon</StyledTableCell>
                                <StyledTableCell >Email</StyledTableCell>
                                <StyledTableCell >Fecha</StyledTableCell>
                                <StyledTableCell >Productos</StyledTableCell>
                                <StyledTableCell >Monto</StyledTableCell>


                            </TableRow>
                        </TableHead>
                        <TableBody className="TableBody">
                            {!this.props.NoData
                                ? Object.keys(this.state.producto).map(i => {
                                    return (
                                        <>
                                        <StyledTableRow hover key={i}>
                                            <StyledTableCell align="left">{this.state.producto[i].cedula}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Nombre}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].direccion}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].email}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Fecha}</StyledTableCell>
                                            <StyledTableCell align="left">
                                                <Icon color="primary"><img onClick={() => this.traerventapro(i)} id="icono" src={li} className="mas"></img></Icon>
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{this.state.producto[i].Total}</StyledTableCell>
                                            <StyledTableCell align="left" style={{display:'none'}} >{
                                            ccyFormat(
                                                sutotal=suma(Number(this.state.producto[i].Total)))}</StyledTableCell>
                                            
                                        
                                        </StyledTableRow>
                                        
                                        </>
                                         
                                    )

                                })
                                : 'sin datos'}
                        </TableBody>
                    </Table>
                    <center>
                    <div style={{backgroundColor:'blue', color:'white'}}>
                    <th >Venta Total </th><br></br>
                    <td>{sutotal}</td>
                    </div>
                    </center>
                </TableContainer>

                <Modal toggle={this.toggle} isOpen={this.state.modalver} className="Modal">
                    <Container  >
                        <ModalHeader> <Typography color="primary" className="titulos" variant="h6" gutterBottom>Productos</Typography></ModalHeader>
                        <ModalBody>
                            <br></br>
                            <TableContainer className="contenedorTabla" id="tabla">
                                <Table className="tabla" >
                                    <StyledTableRow hover key={this.state.data.id}>
                                        <TableHead >
                                            <StyledTableRow>
                                                <StyledTableCell align="center">Nombre</StyledTableCell>
                                                <StyledTableCell align="center">Detalle</StyledTableCell>
                                                <StyledTableCell align="center">Cantidad</StyledTableCell>
                                                <StyledTableCell align="center">Precio</StyledTableCell>
                                            </StyledTableRow>
                                        </TableHead>
                                        <TableBody>
                                            {Object.keys(this.state.data).map(i => {
                                                return (
                                                    <StyledTableRow>

                                                        <StyledTableCell align="center">{this.state.data[i].NombrePro}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Detalle}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].Cantidad}</StyledTableCell>
                                                        <StyledTableCell align="center">{this.state.data[i].CostoVen}</StyledTableCell>
                                                    </StyledTableRow>
                                                )
                                            })
                                            }
                                        </TableBody>
                                    </StyledTableRow>
                                </Table>
                            </TableContainer>
                        </ModalBody>
                        <ModalFooter className="ModalFooter">
                            <br></br><br></br>
                            <Button variant="contained" color="primary" onClick={() => this.setState({ modalver: false })}>Cancelar</Button>

                        </ModalFooter>
                    </Container>
                </Modal>
                </div>
                <div id="Factura" className="Factura">
                    <Case status="Factura" />
                </div>


            </>
        );
    }
}
export default Preventas;
*/

/*
boton preventa
<Button variant="contained" color="primary" onClick={() => this.enviar()}>Facturar</Button>{"   "}

*/
/*  {!this.props.NoData
                                   ? Object.keys(this.state.data).map(i => {
                                       return(
                                       <StyledTableRow>

                                            <StyledTableCell align="left">{this.state.data.producto[i].NombrePro}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].Detalle}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].Cantidad}</StyledTableCell>
                                            <StyledTableCell align="left">{this.state.data.producto[i].producto[i].CostoVen}</StyledTableCell>
                                        </StyledTableRow>
                                    )
                                    })
                                : 'sin datos'}*/

                                /* FIlTRO UNA FECHA
                                
                                 console.log(this.state.fechasIni);
          console.log(this.state.fechasF);
           var text1 = this.state.fechasIni;
          var text2 = this.state.fechasF;
          const data = Object.values(this.state.producto)
          const newData = data.filter(function (item) {
              const itemFecha = item.Fecha    
              const campo = itemFecha;
              const textData1 = text1;
              const textData2 = text2;
               return campo.indexOf(textData1)>-1
          })
          this.setState({
              producto: newData,
  
          })

*/