import { React, Component } from "react";
import {
  withStyles,
  createStyles,
  createMuiTheme,
} from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import firebase from "firebase/app";
import "firebase/database";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import DeleteRoundedIcon from "@material-ui/icons/DeleteRounded";
import BorderColorRoundedIcon from "@material-ui/icons/BorderColorRounded";
import swal from "sweetalert";
import SearchTwoToneIcon from "@material-ui/icons/SearchTwoTone";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import MaterialTable from "material-table";

class inventario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      term: "sin datos",
      producto: [],
      modalEditar: false,
      form: {
        NombrePro: "",
        CostoProv: "",
        CostoVen: "",
        Stock: "",
        Tipo: "",
        Detalle: "",
        Marca: "",
        Fecha: new Date(),
        status: false,
      },
      id: 0,
    };
  }
  toggle = this.toggle.bind(this);

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
    console.log(this.state.form);
  };
  peticionget = () => {
    firebase
      .database()
      .ref()
      .child("Producto")
      .on("value", (datos) => {
        if (datos.val() !== null) {
          this.setState({ ...this.state.data, data: datos.val() });
          this.setState({ ...this.state.producto, producto: datos.val() });
        } else {
          this.setState({ data: [] });
        }
      });
  };
  componentWillMount() {
    this.peticionget();
  }

  seleccionarProducto = async (canal, id, caso) => {
    await this.setState({ form: canal, id: id });
    caso === "Editar"
      ? this.setState({ modalEditar: true })
      : this.peticionDelete();
  };
  filter(event) {
    var text = event.target.value.toUpperCase();
    console.log(this.state.form);
    const data = this.state.data;
    const newData = data.filter(function (item) {
      const itemid = item.id;
      const itemDataTitle = item.NombrePro.toUpperCase();
      const itemDataDescp = item.Detalle.toUpperCase();
      const itemDataTipo = item.Tipo.toUpperCase();
      const itemDataMarca = item.Marca.toUpperCase();
      const campo =
        itemid +
        " " +
        itemDataTitle +
        " " +
        itemDataDescp +
        " " +
        itemDataMarca +
        " " +
        itemDataTipo;
      const textData = text.toUpperCase();
      return campo.indexOf(textData) > -1;
    });
    this.setState({
      producto: newData,
    });
  }

  peticionDelete = () => {
    if (
      swal({
        title: "¿Estás seguro que deseas eliminar el Producto",
        text: " " + this.state.form.NombrePro + "?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          firebase
            .database()
            .ref("Producto/" + this.state.id)
            .remove();
          swal("Eliminado!", {
            icon: "success",
          });
        } else {
          swal("No se elimino el producto!");
        }
      })
    );
  };
  imprimir() {
    console.log("llego");
    var mywindow = window.open("", "PRINT", "height=400,width=600");
    mywindow.document.write("<html><head>");
    mywindow.document.write(
      "<style>.tabla{width:100%;border-collapse:collapse;margin:16px 0 16px 0;}.tabla th{border:1px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;}.tabla td{border:1px solid #ddd;text-align:left;padding:6px;}</style>"
    );
    mywindow.document.write("</head><body >");
    mywindow.document.write(document.getElementById("tabla").innerHTML);
    mywindow.document.write("</body></html>");
    mywindow.document.close(); // necesario para IE >= 10
    mywindow.focus(); // necesario para IE >= 10
    mywindow.print();
    mywindow.close();
  }
  peticionPut = () => {
    firebase
      .database()
      .ref("Producto/" + this.state.id)
      .set(this.state.form, (error) => {
        this.setState({ data: this.state.form, titleButton: "Editar" });
        console.log(error);
      });
    this.setState({ modalEditar: false });
    window.location.reload();
  };

  render() {
    function ccyFormat(num) {
      return `${num.toFixed(2)}`;
    }
    const theme = createMuiTheme({
      palette: {
        primary: {
          light: "#6103F0",
          main: "#651fff",
          dark: "#6103F0",
        },
        secondary: {
          light: "#ff7961",
          main: "#651fff",
          dark: "#ba000d",
          contrastText: "#000",
        },
      },
    });

    const StyledTableCell = withStyles((theme) =>
      createStyles({
        head: {
          backgroundColor: theme.palette.primary.main,
          color: theme.palette.common.white,
          fontSize: 14,
        },
        body: {
          fontSize: 11,
        },

        root: {
          width: "15%",
        },
      })
    )(TableCell);

    const StyledTableRow = withStyles((theme) =>
      createStyles({
        root: {
          "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
          },
        },
      })
    )(TableRow);
    return (
      <>
      
      
        <div className="busqueda">
        
        <input className="inpbusq"
          value={this.state.text}
          onChange={(text) => this.filter(text)}
        />
        
        <SearchTwoToneIcon color="primary" className="iconoBusqueda" />
        <Button
          onClick={() => this.imprimir()}
          variant="contained"
          color="primary"
          className="botonimp"
          size='small'
          
        >
             Imprimir
        </Button>

        </div>
        
<div className="materialtabla">
        <TableContainer className="contenedorTabla" id="tabla">
          <Table className="tabla">
            <TableHead>
              <TableRow>
                <StyledTableCell>Codigo</StyledTableCell>
                <StyledTableCell>Producto</StyledTableCell>
                <StyledTableCell>Costo Compra</StyledTableCell>
                <StyledTableCell>Stock</StyledTableCell>
                <StyledTableCell>Valor Total</StyledTableCell>
                <StyledTableCell>Tipo</StyledTableCell>
                <StyledTableCell>Detalle</StyledTableCell>
                <StyledTableCell>Marca</StyledTableCell>
                <StyledTableCell>Fecha</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody className="TableBody">
              {!this.props.NoData
                ? Object.keys(this.state.producto).map((i) => {
                    return (
                      <StyledTableRow hover key={i}>
                        <StyledTableCell align="left">
                          {this.state.producto[i].id}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].NombrePro}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].CostoProv}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].Stock}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {ccyFormat(
                            this.state.producto[i].CostoProv *
                              this.state.producto[i].Stock
                          )}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].Tipo}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].Detalle}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].Marca}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {this.state.producto[i].Fecha}
                        </StyledTableCell>
                      </StyledTableRow>
                    );
                  })
                : "sin datos"}
            </TableBody>
          </Table>
        </TableContainer>
        </div>
      </>
    );
  }
}
export default inventario;

/*
 <TableCell align="right"> <Button size="small" variant="outlined" color="primary" >Ver/Editar </Button><Button style={{ marginLeft: "10px" }}
                                    size="small" variant="outlined" color="secondary">Eliminar</Button>
                                </TableCell>
                                className="tabla"
<table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Precio Proveedor</th>
                        <th scope="col"> Precio Venta</th>
                        <th scope="col"> Stock</th>
                        <th scope="col">xd</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th>{this.state.name}</th>
                    <th>{this.state.detalle}</th>
                    <th>xd</th>
                    <th>xd</th>
                </tr>
                </tbody>
            </table>
*/
/*componentWillMount() {
        var indice = firebase.database().ref().child('Producto').push.length;
        console.log(indice);
        for (var id = 0; id <= indice; id++) {
            console.log(id);
            const nameref = firebase.database().ref().child('Producto').child(id).child('producto')
            nameref.on('value', (snapshot) => {
                console.log(snapshot.val())
                this.setState({
                    id: snapshot.val().id,
                    name: snapshot.val().NombrePro,
                    preciopro: snapshot.val().CostoProv,
                    preciopub: snapshot.val().CostoVen,
                    stock: snapshot.val().Stock,
                    detalle: snapshot.val().Detalle
                })
            })
        }
    } */
/*<Modal isOpen={this.state.modalEditar}>
                <ModalHeader>Editar Registro</ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <label>Nombre Producto: </label>
                        <br />
                        <input type="text" className="form-control" name="NombrePro" onChange={this.handleChange} value={this.state.data['0'].producto.NombrePro} />
                        <br />
                        <label>Precio Proveedor </label>
                        <br />
                        <input type="text" className="form-control" name="CostoProv" onChange={this.handleChange} value={this.state.data['0'].producto.CostoProv} />
                        <br />
                        <label>Precio Final </label>
                        <br />
                        <input type="text" className="form-control" name="CostoVen" onChange={this.handleChange} value={this.state.data['0'].producto.CostoVen} />
                        <br />
                        <label>Stock </label>
                        <br />
                        <input type="text" className="form-control" name="Stock" onChange={this.handleChange} value={this.state.data['0'].producto.Stock} />
                        <label>Tipo </label>
                        <br />
                        <input type="text" className="form-control" name="Tipo" onChange={this.handleChange} value={this.state.data['0'].producto.Tipo} />
                        <br />
                        <label>Detalle </label>
                        <br />
                        <input type="text" className="form-control" name="Detalle" onChange={this.handleChange} value={this.state.data['0'].producto.Detalle} />
                        <br />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-primary" onClick={() => this.peticionPut()}>Editar</button>{"   "}
                    <button className="btn btn-danger" onClick={() => this.setState({ modalEditar: false })}>Cancelar</button>
                </ModalFooter>
            </Modal> */
