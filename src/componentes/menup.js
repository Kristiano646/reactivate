import React from "react";
import clsx from "clsx";

import { makeStyles, useTheme } from "@material-ui/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import Case from "./case";
import venta from "../logos/vender.png";
import preventa from "../logos/Preorder.png";
import inv from "../logos/inventario.png";
import ped from "../logos/pedido.png";
import ven from "../logos/vendedor.png";
import prv from "../logos/proveedor.png";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Badge from "@material-ui/core/Badge";
import firebase from "../util/firebase";
import firebaseConfig from "../util/firebase";
import Rutas from "../rutas/rutas";

const drawerWidth = 240;

export default function PersistentDrawerLeft() {
  const [date] = React.useState(new Date());

  const formatDate = (date) => {
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    return date.toLocaleDateString(undefined, options);
  };

  return (
    <>
    <div className="contenedorSec" >
      <div className="titulo" id="titulo">
       <div className="fecha">{formatDate(date)}&nbsp;&nbsp;&nbsp;Famey,&nbsp;siempre pensando en usted</div>
      </div>
      </div>
    </>
  );
}
