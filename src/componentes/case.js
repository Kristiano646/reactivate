import React from "react";
import Inventario from "./section_components/inventario";
import Agregar from "./section_components/agregarProd";
import Vendedor from "./section_components/Vendedor";
import AgregarV from "./section_components/AgregarVendedor";
import ListarV from "./section_components/ListarVendedor";
import AgregarPrv from "./section_components/AgregaProv";
import ListarPrv from "./section_components/ListarProv";
import Proveedor from "./section_components/Proveedor";
import Reportes from "./section_components/Reportes";
import Precios from "./section_components/Precios";
import Stock from "./section_components/RepStock";
import Ver from "./section_components/VerCompra";
import Fact from "./section_components/facturar";
import Psv from "./section_components/PostVenta";
import RepVen from "./section_components/RepVentas";
import Preventa from "../componentes/section_components/Preventa";
import FactPre from "../componentes/section_components/facturaPre";
import Section from '../componentes/section';
const Case = ({ status }) => {
  switch (status) {
    case "PostVentas":
      return <Psv />;
    case "Preventa":
      return <Preventa />;
    case "Reportes":
      return <Reportes />;
    case "Compras":
      return (
        <>
          <Agregar />
          <br></br> <h1>Visualizar Compras</h1> <Ver />
        </>
      );
    case "Vendedor":
      return <Vendedor />;
    case "ListarV":
      return <ListarV />;
    case "AgregarV":
      return <AgregarV />;
    case "Proveedor":
      return <Proveedor />;
    case "AgregaProv":
      return <AgregarPrv />;
    case "ListarProv":
      return <ListarPrv />;
    case "Inventarios":
      return <Inventario />;
    case "Precios":
      return <Precios />;
    case "Stock":
      return <Stock />;
    case "Factura":
      return <Fact />;

    case "Ventas":
      return <RepVen />;
    case "FacturaPre":
      return <FactPre />;
    
    default:
      return null;
  }
};

export default Case;
