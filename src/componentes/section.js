import React, { useState, useEffect } from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";

import clsx from "clsx";
import Divider from "@material-ui/core/Divider";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Case from "./case";
import venta from "../logos/vender.png";
import preventa from "../logos/Preorder.png";
import inv from "../logos/inventario.png";
import ped from "../logos/pedido.png";
import ven from "../logos/vendedor.png";
import prv from "../logos/proveedor.png";
import Drawer from "@material-ui/core/Drawer";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import firebase from "../util/firebase";
import firebaseConfig from "../util/firebase";
import Rutas from "../rutas/rutas";
import ReactDOM from "react-dom";
import { render } from "@testing-library/react";
import Ini from "../vistas/Inicio";
import Container from '@mui/material/Container';

const drawerWidth = 240;
const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  iconos: {
    display: "flex",
    height: "4%",
    marginTop: "-1%",
    marginLeft: "60%",
  },
  AppBar: {
    display: "flex",
    float: "right",
    marginTop: "5%",
    height: "4%",
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    background: theme.palette.primary,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));
export default function Section() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [val, setVal] = React.useState("");
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const theme = useTheme();
  const [email, setEmail] = React.useState(null);
  var [data, setData] = React.useState([]);
  var [count, setCount] = React.useState(0);
  const handleChange = (valo) => {
    setVal(valo);
   console.log(this.props.opc)
     
  };
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <>
    
     <IconButton
        onClick={handleDrawerOpen}
        size="large"
        edge="start"
        color="inherit"
        aria-label="open drawer"
        sx={{ mr: 1 }}
      >
        <MenuIcon />
      </IconButton>
     
      <Drawer className={classes.drawer} anchor="left" open={open}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === "ltr" ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>

        <Divider />
        <List>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={venta} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Venta"
              onClick={() => handleChange("PostVentas")}
            />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={preventa} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Preventa"
              onClick={() => handleChange("Preventa")}
            />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={inv} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Reportes"
              onClick={() => handleChange("Reportes")}
            />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={ped} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Compras"
              onClick={() => handleChange("Compras")}
            />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={ven} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Vendedor"
              onClick={() => handleChange("Vendedor")}
            />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              {" "}
              <img src={prv} className="imglog" />
            </ListItemIcon>
            <ListItemText
              primary="Proveedor"
              onClick={() => handleChange("Proveedor")}
            />
          </ListItem>
        </List>
        <Divider />
      </Drawer>
      
    </>
  );
}
