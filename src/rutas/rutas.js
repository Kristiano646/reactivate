import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Home from '../vistas/Inicio';
import Login from '../vistas/Login';
import firebase from "../util/firebase";



class Router extends Component {
    constructor(props) {
        super(props);
        this.state = {
            val: false
        }

    }

    usuario = firebase.auth().onAuthStateChanged(authUser => {
        authUser
            ? localStorage.setItem('authUser', JSON.stringify(authUser),
                console.log(authUser),
                this.setState({ val: true })
            )
            : localStorage.removeItem('authUser')
        console.log(authUser)
    });

    render() {
        console.log(this.state.val);
        if (this.state.val !== false) {
            return (
                <BrowserRouter>
                        <Home/>
                </BrowserRouter>
            );

        } else {
            return(
                <BrowserRouter>
                    <Login/>
                </BrowserRouter>
            );
        }
    }
}
export default Router;

//window.location = '/inicio';
/*export const SIGN_UP = "/signup";
export const SIGN_IN = "/signin";
export const LANDING = "/";
export const HOME = "/Home";
export const ACCOUNT = "/account";
export const PASSWORD_FORGET = "/pw-forget";*/
