import React from "react";
import {Route, Redirect } from "react-router-dom";
import firebase from '../util/firebase';

const PrivateRoute=({component: Component,auth,...rest}) =>(
<Route
{...rest}
render={props=>
firebase.auth().isAuthenticated==false?(
<Component {...props}/>
) : (
  <Redirect to="/"/>,
  firebase.auth().isAuthenticated==true?(
  <Component {...props}/>
  ) : (
    <Redirect to="/"/>
  )
)




}/>

  
  



);
export default PrivateRoute; 


